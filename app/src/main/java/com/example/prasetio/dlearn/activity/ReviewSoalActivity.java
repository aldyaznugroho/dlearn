package com.example.prasetio.dlearn.activity;

import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Toast;

import com.example.prasetio.dlearn.R;
import com.example.prasetio.dlearn.model.HistorySoal;
import com.example.prasetio.dlearn.utils.MainConst;
import com.example.prasetio.dlearn.utils.SharedPrefCustom;
import com.example.prasetio.dlearn.view.adapter.ReviewSoalStepAdapter;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.kennyc.view.MultiStateView;
import com.stepstone.stepper.StepperLayout;
import com.stepstone.stepper.VerificationError;

import butterknife.BindView;
import butterknife.ButterKnife;
import timber.log.Timber;

public class ReviewSoalActivity extends AppCompatActivity
        implements StepperLayout.StepperListener, ValueEventListener {

    @BindView(R.id.review_soal_toolbar) Toolbar toolbar;
    @BindView(R.id.review_soal_state_view) MultiStateView stateView;
    @BindView(R.id.review_soal_stepper) StepperLayout stepperLayout;

    private DatabaseReference historyDbRef;
    private ReviewSoalStepAdapter stepAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_review_soal);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        init();
        setTitle("Review");
    }

    private void init() {
        initToolbar();
        initStateView();
        initStepper();
        initFirebaseDatabase();
    }

    private void initToolbar() {
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        Drawable navigationIcon = toolbar.getNavigationIcon();
        Drawable overflowIcon = toolbar.getOverflowIcon();
        if (overflowIcon != null && navigationIcon != null) {
            navigationIcon.mutate();
            navigationIcon.setColorFilter(
                    ContextCompat.getColor(this, R.color.color_white),
                    PorterDuff.Mode.SRC_ATOP);

            overflowIcon.mutate();
            overflowIcon.setColorFilter(
                    ContextCompat.getColor(this, R.color.color_white),
                    PorterDuff.Mode.SRC_ATOP);
        }
    }

    private void initStateView() {
        stateView.setViewState(MultiStateView.VIEW_STATE_LOADING);
        stateView.getView(MultiStateView.VIEW_STATE_ERROR).findViewById(R.id.state_retry_button)
                .setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        callHistorySoalApi();
                    }
                });
    }

    private void initStepper() {
        stepAdapter = new ReviewSoalStepAdapter(this);
        stepperLayout.setAdapter(stepAdapter);
        stepperLayout.setListener(this);
    }

    private void initFirebaseDatabase() {
        String historyId = getIntent().getExtras().getString(MainConst.KEY_REVIEW_FIREBASE_RANDOM_KEY);
        Integer userId = SharedPrefCustom.getInstance(this).getSharedPrefUser().getUserId();

        if (historyId != null) {
            historyDbRef = FirebaseDatabase.getInstance()
                    .getReference(MainConst.KEY_DB_SOAL_HISTORY)
                    .child("" + userId)
                    .child(historyId);
        }
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    protected void onStart() {
        super.onStart();
        callHistorySoalApi();
    }

    @Override
    public void onCompleted(View completeButton) {
        finish();
    }

    @Override
    public void onError(VerificationError verificationError) {
    }

    @Override
    public void onStepSelected(int newStepPosition) {
    }

    @Override
    public void onReturn() {
    }

    private void callHistorySoalApi() {
        historyDbRef.addValueEventListener(this);
    }

    @Override
    public void onDataChange(DataSnapshot dataSnapshot) {
        if (dataSnapshot.exists() && dataSnapshot.getValue() != null) {
            stateView.setViewState(MultiStateView.VIEW_STATE_CONTENT);

            if (!dataSnapshot.child("soalResultList").exists()) {
                Toast.makeText(this, "Tidak ada soal yang selesaikan", Toast.LENGTH_SHORT).show();
                finish();
            }

            try {
                HistorySoal historySoal = dataSnapshot.getValue(HistorySoal.class);
                if (historySoal != null) {
                    Timber.d("total soalResult: " + historySoal.getSoalResultList().size());
                    if (historySoal.getSoalResultList().size() == 1) {
                        stepperLayout.setShowBottomNavigation(false);
                    } else if (historySoal.getSoalResultList().size() == 0) {
                        Toast.makeText(this, "Tidak ada soal yang selesaikan", Toast.LENGTH_SHORT).show();
                        finish();
                    }

                    stepAdapter.addAllSoalResultItem(historySoal.getSoalResultList());
                } else {
                    stateView.setViewState(MultiStateView.VIEW_STATE_EMPTY);
                }
            } catch (Exception e) {
                Timber.e("Error: " + e.getMessage());
                stateView.setViewState(MultiStateView.VIEW_STATE_ERROR);
            }
        }
    }

    @Override
    public void onCancelled(DatabaseError databaseError) {
        Timber.d("FIREBASE DB ERROR: " + databaseError.getMessage());
    }
}
