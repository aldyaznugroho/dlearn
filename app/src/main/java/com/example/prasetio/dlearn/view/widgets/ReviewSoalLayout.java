package com.example.prasetio.dlearn.view.widgets;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.example.prasetio.dlearn.R;
import com.example.prasetio.dlearn.activity.ViewPhotoActivity;
import com.example.prasetio.dlearn.model.JawabanSoalLog;
import com.example.prasetio.dlearn.model.OpsiJawaban;
import com.example.prasetio.dlearn.model.Soal;
import com.example.prasetio.dlearn.model.SoalResult;
import com.example.prasetio.dlearn.utils.MainConst;
import com.example.prasetio.dlearn.view.adapter.CustomRecyclerAdapter;
import com.example.prasetio.dlearn.view.adapter.OpsiSoalAdapter;
import com.github.stephenvinouze.advancedrecyclerview_core.adapters.RecyclerAdapter;
import com.stepstone.stepper.BlockingStep;
import com.stepstone.stepper.Step;
import com.stepstone.stepper.StepperLayout;
import com.stepstone.stepper.VerificationError;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Aldyaz on 8/1/2017.
 */

public class ReviewSoalLayout extends LinearLayout implements Step {

    @BindView(R.id.soal_case_tv) TextView soalCaseText;
    @BindView(R.id.soal_case_iv) CircleImageView soalCaseIv;
    @BindView(R.id.soal_jawaban_dipilih) TextView jawabanDipilihText;
    @BindView(R.id.soal_jawaban_benar) TextView jawabanBenarText;
    @BindView(R.id.soal_pembahasan) TextView jawabanPembahasanText;

    private SoalResult soalResult;
    private int soalPosition;

    public ReviewSoalLayout(Context context, SoalResult soalResult, int position) {
        super(context);
        this.soalResult = soalResult;
        this.soalPosition = position;
        initViews();
    }

    public ReviewSoalLayout(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public ReviewSoalLayout(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public ReviewSoalLayout(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    private void initViews() {
        View view = inflate(getContext(), R.layout.layout_review_soal, this);
        ButterKnife.bind(this, view);
        bind();
    }

    private void bind() {
        if (soalResult != null) {
            bindSoalResult();
        }
    }

    private void bindSoalResult() {
        String jawabanDipilih = soalResult.getJawabanDipilih();
        String jawabanBenar = soalResult.getJawabanBenar();
        String jawabanPembahasan = soalResult.getPembahasanSoal();

        if (soalResult.getPicture() == null || soalResult.getPicture().equals("")) {
            soalCaseIv.setVisibility(GONE);
        } else {
            Glide.with(getContext())
                    .load(soalResult.getPicture())
                    .diskCacheStrategy(DiskCacheStrategy.RESULT)
                    .skipMemoryCache(true)
                    .dontAnimate()
                    .into(soalCaseIv);

            soalCaseIv.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (soalCaseIv != null) {
                        Bundle bundle = new Bundle();
                        bundle.putString(MainConst.KEY_INTENT_SOAL_VIEW_PHOTO, soalResult.getPicture());

                        Intent viewPhotoIntent = new Intent(getContext(), ViewPhotoActivity.class);
                        viewPhotoIntent.putExtras(bundle);
                        getContext().startActivity(viewPhotoIntent);
                    }
                }
            });
        }

        soalCaseText.setText(soalResult.getSoal());

        if (jawabanDipilih != null && !jawabanDipilih.equalsIgnoreCase("")) {
            jawabanDipilihText.setText(jawabanDipilih);
        } else {
            jawabanDipilihText.setText(getContext().getString(R.string.tidak_ada));
        }

        if (jawabanBenar != null && !jawabanBenar.equalsIgnoreCase("")) {
            jawabanBenarText.setText(jawabanBenar);
        } else {
            jawabanBenarText.setText(getContext().getString(R.string.tidak_ada));
        }

        if (jawabanDipilih != null && jawabanDipilih.equalsIgnoreCase(jawabanBenar)) {
            jawabanDipilihText.setTextColor(ContextCompat.getColor(getContext(), R.color.color_green_600));
            jawabanBenarText.setTextColor(ContextCompat.getColor(getContext(), R.color.color_green_600));
        } else {
            jawabanDipilihText.setTextColor(ContextCompat.getColor(getContext(), R.color.color_red_700));
            jawabanBenarText.setTextColor(ContextCompat.getColor(getContext(), R.color.color_green_600));
        }

        if (jawabanPembahasan != null && !jawabanPembahasan.equalsIgnoreCase("")) {
            jawabanPembahasanText.setText(jawabanPembahasan);
        } else {
            jawabanPembahasanText.setText(String.valueOf("Tidak ada pembahasan"));
        }
    }

    @Override
    public VerificationError verifyStep() {
        return null;
    }

    @Override
    public void onSelected() {

    }

    @Override
    public void onError(@NonNull VerificationError error) {

    }
}
