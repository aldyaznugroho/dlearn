package com.example.prasetio.dlearn.activity;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.example.prasetio.dlearn.R;
import com.example.prasetio.dlearn.api.ApiAdapter;
import com.example.prasetio.dlearn.api.ApiService;
import com.example.prasetio.dlearn.model.HistorySoal;
import com.example.prasetio.dlearn.model.JawabanSoalLog;
import com.example.prasetio.dlearn.model.OpsiJawaban;
import com.example.prasetio.dlearn.model.ScoreResponse;
import com.example.prasetio.dlearn.model.ScoreBody;
import com.example.prasetio.dlearn.model.Soal;
import com.example.prasetio.dlearn.model.SoalData;
import com.example.prasetio.dlearn.utils.MainConst;
import com.example.prasetio.dlearn.utils.SharedPrefCustom;
import com.example.prasetio.dlearn.view.adapter.OpsiSoalAdapter;
import com.github.stephenvinouze.advancedrecyclerview_core.adapters.RecyclerAdapter;
import com.github.stephenvinouze.advancedrecyclerview_core.callbacks.ClickCallback;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.kennyc.view.MultiStateView;
import com.stepstone.stepper.StepperLayout;
import com.stepstone.stepper.VerificationError;

import java.net.SocketTimeoutException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.UUID;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.iwgang.countdownview.CountdownView;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

public class SoalActivity extends AppCompatActivity {

    @BindView(R.id.soal_toolbar) Toolbar toolbar;
    @BindView(R.id.soal_state_view) MultiStateView stateView;
    @BindView(R.id.soal_countdown) CountdownView countdownView;
    @BindView(R.id.soal_case_number) TextView soalNumberText;
    @BindView(R.id.soal_case_tv) TextView soalCaseText;
    @BindView(R.id.soal_case_iv) CircleImageView soalCaseIv;
    @BindView(R.id.opsi_rv) RecyclerView opsiRecyclerView;

    private ProgressDialog progressDialog;
    private OpsiSoalAdapter opsiAdapter;

    private List<Soal> itemSoalList;
    private ApiService apiService;
    private DatabaseReference historyDbRef;
    private int soalIndex = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_soal);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        init();
        setTitle("Soal");
    }

    private void init() {
        initToolbar();
        initStateView();
        initProgressDialog();
        initCountdownView();
        initFirebaseDatabase();
        initRetrofit();
    }

    private void initToolbar() {
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        Drawable navigationIcon = toolbar.getNavigationIcon();
        Drawable overflowIcon = toolbar.getOverflowIcon();
        if (overflowIcon != null && navigationIcon != null) {
            navigationIcon.mutate();
            navigationIcon.setColorFilter(
                    ContextCompat.getColor(this, R.color.color_white),
                    PorterDuff.Mode.SRC_ATOP);

            overflowIcon.mutate();
            overflowIcon.setColorFilter(
                    ContextCompat.getColor(this, R.color.color_white),
                    PorterDuff.Mode.SRC_ATOP);
        }
    }

    private void initStateView() {
        stateView.getView(MultiStateView.VIEW_STATE_ERROR).findViewById(R.id.state_retry_button)
                .setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        callSoalApi();
                    }
                });
    }

    private void initProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Mohon tunggu ...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCancelable(false);
    }

    private void initCountdownView() {
        countdownView.setVisibility(View.GONE);
        countdownView.setOnCountdownEndListener(new CountdownView.OnCountdownEndListener() {
            @Override
            public void onEnd(CountdownView cv) {
                showTimerEndDialog();
            }
        });
    }

    private void initFirebaseDatabase() {
        historyDbRef = FirebaseDatabase.getInstance()
                .getReference(MainConst.KEY_DB_SOAL_HISTORY);
    }

    private void initRetrofit() {
        apiService = ApiAdapter.getInstance().getRetrofit(MainConst.BASE_URL).create(ApiService.class);
        callSoalApi();
    }

    @Override
    public void onBackPressed() {
        if (stateView.getViewState() == MultiStateView.VIEW_STATE_EMPTY) {
            backToLevelActivity();
        } else {
            showDiscardFormDialog();
        }
    }

    private void showDiscardFormDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        AlertDialog dialog;

        builder.setMessage("Anda yakin?");
        builder.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                for (int prefIndex = 0; prefIndex < itemSoalList.size(); prefIndex++) {
                    SharedPrefCustom.getInstance(SoalActivity.this).removeSharedPrefJawabanSoalLog(prefIndex);
                }
                finish();
            }
        });
        builder.setNegativeButton("Batal", null);
        dialog = builder.create();
        dialog.show();
    }

    private void showTimerEndDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        AlertDialog dialog;

        builder.setMessage("Waktu pengerjaan telah habis");
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                processingSoalResult();
            }
        });
        dialog = builder.create();
        dialog.show();
    }

    private void processingSoalResult() {
        progressDialog.show();

        final ArrayList<JawabanSoalLog> jawabanSoalLogArrayList = new ArrayList<>();
        final Integer userId = SharedPrefCustom.getInstance(this).getSharedPrefUser().getUserId();

        for (int prefIndex = 0; prefIndex < itemSoalList.size(); prefIndex++) {
            JawabanSoalLog jawabanSoalLog = SharedPrefCustom
                    .getInstance(SoalActivity.this)
                    .getSharedPrefJawabanSoalLog(prefIndex);

            jawabanSoalLogArrayList.add(jawabanSoalLog);
        }

        for (int x = 0; x < jawabanSoalLogArrayList.size(); x++) {
            Integer testSoalId = jawabanSoalLogArrayList.get(x).getSoal_id();
            String testJwbDipilih = jawabanSoalLogArrayList.get(x).getJawaban_dipilih();
            String testJwbBenar = jawabanSoalLogArrayList.get(x).getJawaban_benar();

            Timber.d("SoalId: " + testSoalId);
            Timber.d("JwbDipilih: " + testJwbDipilih);
            Timber.d("JwbBenar: " + testJwbBenar);
        }

        final ScoreBody scoreBody = new ScoreBody();
        scoreBody.setMember_id(userId);
        scoreBody.setMatpel_id(getIntent().getExtras().getInt(MainConst.KEY_INTENT_MATPEL_ID));
        scoreBody.setKelas_id(getIntent().getExtras().getInt(MainConst.KEY_INTENT_KELAS_ID));
        scoreBody.setLevel_id(getIntent().getExtras().getInt(MainConst.KEY_INTENT_LEVEL_ID));
        scoreBody.setJenjang_id(getIntent().getExtras().getInt(MainConst.KEY_INTENT_JENJANG_ID));
        scoreBody.setLog_jawaban(jawabanSoalLogArrayList);

        Call<ScoreResponse> scoreResponseCall = apiService.postScore(scoreBody);
        scoreResponseCall.enqueue(new Callback<ScoreResponse>() {
            @Override
            public void onResponse(Call<ScoreResponse> call, Response<ScoreResponse> response) {
                final ScoreResponse scoreResponse = response.body();

                if (response.isSuccessful()) {
                    final String historyId = UUID.randomUUID().toString();
                    DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss a", Locale.getDefault());

                    HistorySoal historySoal = new HistorySoal();
                    historySoal.setHistorySoalId(historyId);
                    historySoal.setUserId(userId);
                    historySoal.setJenjang(getIntent().getExtras().getString(MainConst.KEY_REVIEW_SOAL_JENJANG));
                    historySoal.setKelas(getIntent().getExtras().getString(MainConst.KEY_REVIEW_SOAL_KELAS));
                    historySoal.setMatpel(getIntent().getExtras().getString(MainConst.KEY_REVIEW_SOAL_MATPEL));
                    historySoal.setLevel(getIntent().getExtras().getString(MainConst.KEY_REVIEW_SOAL_LEVEL));
                    historySoal.setSoalResultList(scoreResponse.getSoalResultList());
                    historySoal.setScore(scoreResponse.getScore());
                    historySoal.setTimestamp(dateFormat.format(new Date()));

                    historyDbRef.child("" + userId)
                            .child(historyId)
                            .setValue(historySoal)
                            .addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    Timber.d("SUKSES");

                                    progressDialog.dismiss();

                                    for (int prefIndex = 0; prefIndex < itemSoalList.size(); prefIndex++) {
                                        SharedPrefCustom.getInstance(SoalActivity.this).removeSharedPrefJawabanSoalLog(prefIndex);
                                    }

                                    Bundle bundle = new Bundle();
                                    bundle.putInt(MainConst.KEY_REVIEW_SOAL_SCORE, scoreResponse.getScore());
                                    bundle.putString(MainConst.KEY_REVIEW_FIREBASE_RANDOM_KEY, historyId);
                                    bundle.putString(MainConst.KEY_REVIEW_SOAL_JENJANG, getIntent().getExtras().getString(MainConst.KEY_REVIEW_SOAL_JENJANG));
                                    bundle.putString(MainConst.KEY_REVIEW_SOAL_KELAS, getIntent().getExtras().getString(MainConst.KEY_REVIEW_SOAL_KELAS));
                                    bundle.putString(MainConst.KEY_REVIEW_SOAL_MATPEL, getIntent().getExtras().getString(MainConst.KEY_REVIEW_SOAL_MATPEL));
                                    bundle.putString(MainConst.KEY_REVIEW_SOAL_LEVEL, getIntent().getExtras().getString(MainConst.KEY_REVIEW_SOAL_LEVEL));

                                    Intent soalResultIntent = new Intent(SoalActivity.this, SoalResultActivity.class);
                                    soalResultIntent.putExtras(bundle);
                                    startActivity(soalResultIntent);
                                    finish();
                                }
                            }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            progressDialog.dismiss();
                            Timber.e("GAGAL");
                            Timber.e("EXCEPTION: " + e.getMessage());
                        }
                    });
                } else {
                    progressDialog.dismiss();

                    Timber.e("MESSAGE: " + response.message());
                    Timber.e("RESPONSE CODE " + response.code());
                    Toast.makeText(SoalActivity.this, "Error response", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ScoreResponse> call, Throwable t) {
                if (t instanceof SocketTimeoutException) {
                    Toast.makeText(SoalActivity.this, "Request timeout. Please try again", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getApplicationContext(), "Network connecting error", Toast.LENGTH_SHORT).show();
                }

                progressDialog.dismiss();

                Timber.e("ERROR: " + t.getMessage());
            }
        });
    }

    private void callSoalApi() {
        stateView.setViewState(MultiStateView.VIEW_STATE_LOADING);

        Integer kelasId = getIntent().getExtras().getInt(MainConst.KEY_INTENT_KELAS_ID);
        Integer matpelId = getIntent().getExtras().getInt(MainConst.KEY_INTENT_MATPEL_ID);
        Integer levelId = getIntent().getExtras().getInt(MainConst.KEY_INTENT_LEVEL_ID);

        Call<SoalData> soalDataCall = apiService.getSoal(kelasId, matpelId, levelId);
        soalDataCall.enqueue(new Callback<SoalData>() {
            @Override
            public void onResponse(Call<SoalData> call, Response<SoalData> response) {
                itemSoalList = response.body().getListSoal();

                if (response.isSuccessful()) {
                    if (itemSoalList.size() > 0) {
                        stateView.setViewState(MultiStateView.VIEW_STATE_CONTENT);
                        bindSoal(itemSoalList.get(0), 0);
                        countdownView.setVisibility(View.VISIBLE);
                        countdownView.start(1800000);
                    } else {
                        stateView.setViewState(MultiStateView.VIEW_STATE_EMPTY);
                    }
                } else {
                    Timber.e("MESSAGE: " + response.message());
                    Timber.e("RESPONSE CODE " + response.code());
                    Toast.makeText(SoalActivity.this, "Error response", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<SoalData> call, Throwable t) {
                if (t instanceof SocketTimeoutException) {
                    Toast.makeText(SoalActivity.this, "Request timeout. Please try again", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getApplicationContext(), "Network connecting error", Toast.LENGTH_SHORT).show();
                }

                stateView.setViewState(MultiStateView.VIEW_STATE_ERROR);

                Timber.e("ERROR: " + t.getMessage());
            }
        });
    }

    private void bindSoal(final Soal soal, int soalIndex) {
        if (soal != null) {
            Integer nomorSoal = soalIndex + 1;
            String nomorSoalFormat = "Nomor " + nomorSoal;

            soalNumberText.setText(nomorSoalFormat);
            soalCaseText.setText(soal.getSoal());

            if (soal.getPicture() == null || soal.getPicture().equals("")) {
                soalCaseIv.setVisibility(View.GONE);
            } else {
                soalCaseIv.setVisibility(View.VISIBLE);

                Glide.with(SoalActivity.this)
                        .load(soal.getPicture())
                        .skipMemoryCache(true)
                        .dontAnimate()
                        .into(soalCaseIv);

                soalCaseIv.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (soalCaseIv != null) {
                            Bundle bundle = new Bundle();
                            bundle.putString(MainConst.KEY_INTENT_SOAL_VIEW_PHOTO, soal.getPicture());

                            Intent viewPhotoIntent = new Intent(SoalActivity.this, ViewPhotoActivity.class);
                            viewPhotoIntent.putExtras(bundle);
                            startActivity(viewPhotoIntent);
                        }
                    }
                });
            }

            opsiAdapter = new OpsiSoalAdapter(SoalActivity.this);
            opsiAdapter.setItems(soal.getOpsiJawaban());
            opsiAdapter.setChoiceMode(RecyclerAdapter.ChoiceMode.SINGLE_CHOICE);

            opsiRecyclerView.setLayoutManager(new LinearLayoutManager(SoalActivity.this));
            opsiRecyclerView.setNestedScrollingEnabled(false);
            opsiRecyclerView.setHasFixedSize(true);
            opsiRecyclerView.setItemAnimator(null);
            opsiRecyclerView.setAdapter(opsiAdapter);

            opsiKlik(soal);
        }
    }

    private void opsiKlik(final Soal soal) {
        opsiAdapter.setClickCallback(new ClickCallback() {
            @Override
            public void onItemClick(View view, int i) {
                OpsiJawaban opsi = opsiAdapter.getItems().get(i);

                JawabanSoalLog jawabanSoalLog = new JawabanSoalLog();
                jawabanSoalLog.setSoal_id(soal.getSoalId());
                jawabanSoalLog.setJawaban_dipilih(opsi.getText());
                jawabanSoalLog.setJawaban_benar(soal.getJawabanBenar());

                SharedPrefCustom.getInstance(SoalActivity.this)
                        .putSharedPrefJawabanSoalLog(jawabanSoalLog, soalIndex);

                showJawabanBenarDialog(soal, opsi.getText());
            }
        });
    }

    private void showJawabanBenarDialog(Soal soal, String jawabanDipilih) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        AlertDialog dialog;

        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.dialog_custom_soal, null);

        TextView jwbDipilihTxt = (TextView) dialogView.findViewById(R.id.jwb_dipilih_txt);
        TextView jwbBenarTxt = (TextView) dialogView.findViewById(R.id.jwb_benar_txt);
        TextView pembahasanTxt = (TextView) dialogView.findViewById(R.id.pembahasan_txt);

        jwbDipilihTxt.setText(String.valueOf("Jawaban dipilih: " + jawabanDipilih));
        jwbBenarTxt.setText(String.valueOf("Jawaban benar: " + soal.getJawabanBenar()));
        if (soal.getPembahasanSoal() != null && !soal.getPembahasanSoal().equalsIgnoreCase("")) {
            pembahasanTxt.setText(soal.getPembahasanSoal());
        } else {
            pembahasanTxt.setText(String.valueOf("Tidak ada pembahasan"));
        }

        builder.setView(dialogView);
        builder.setCancelable(false);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if (soalIndex < itemSoalList.size() - 1) {
                    soalIndex++;
                    Soal soalBerikut = itemSoalList.get(soalIndex);
                    bindSoal(soalBerikut, soalIndex);
                } else {
                    processingSoalResult();
                }
            }
        });
        dialog = builder.create();
        dialog.show();
    }

    private void backToLevelActivity() {
        Bundle bundle = new Bundle();
        bundle.putInt(MainConst.KEY_INTENT_JENJANG_ID, getIntent().getExtras().getInt(MainConst.KEY_INTENT_JENJANG_ID));
        bundle.putInt(MainConst.KEY_INTENT_KELAS_ID, getIntent().getExtras().getInt(MainConst.KEY_INTENT_KELAS_ID));
        bundle.putInt(MainConst.KEY_INTENT_MATPEL_ID, getIntent().getExtras().getInt(MainConst.KEY_INTENT_MATPEL_ID));

        bundle.putString(MainConst.KEY_INTENT_MATPEL_NAMA, getIntent().getExtras().getString(MainConst.KEY_INTENT_MATPEL_NAMA));
        bundle.putString(MainConst.KEY_INTENT_JENJANG_NAMA,
                getIntent().getExtras().getString(MainConst.KEY_INTENT_JENJANG_NAMA));
        bundle.putString(MainConst.KEY_INTENT_KELAS_NAMA,
                getIntent().getExtras().getString(MainConst.KEY_INTENT_KELAS_NAMA));

        Intent levelIntent = new Intent(SoalActivity.this, LevelActivity.class);
        levelIntent.putExtras(bundle);
        startActivity(levelIntent);
        finish();
    }
}
