package com.example.prasetio.dlearn.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Aldyaz on 7/28/2017.
 */

public class JawabanSoalLog {

    private Integer soal_id;
    private String jawaban_dipilih;
    private String jawaban_benar;

    public JawabanSoalLog() {
    }

    public JawabanSoalLog(Integer soal_id, String jawaban_dipilih, String jawaban_benar) {
        this.soal_id = soal_id;
        this.jawaban_dipilih = jawaban_dipilih;
        this.jawaban_benar = jawaban_benar;
    }

    public Integer getSoal_id() {
        return soal_id;
    }

    public void setSoal_id(Integer soal_id) {
        this.soal_id = soal_id;
    }

    public String getJawaban_dipilih() {
        return jawaban_dipilih;
    }

    public void setJawaban_dipilih(String jawaban_dipilih) {
        this.jawaban_dipilih = jawaban_dipilih;
    }

    public String getJawaban_benar() {
        return jawaban_benar;
    }

    public void setJawaban_benar(String jawaban_benar) {
        this.jawaban_benar = jawaban_benar;
    }
}
