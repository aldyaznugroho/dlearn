package com.example.prasetio.dlearn.model;

import java.util.List;


/**
 * Created by Aldyaz on 7/31/2017.
 */

public class HistorySoal {

    private String historySoalId;
    private Integer userId;
    private String jenjang;
    private String kelas;
    private String matpel;
    private String level;
    private List<SoalResult> soalResultList;
    private Integer score;
    private String timestamp;

    public HistorySoal() {
    }

    public String getHistorySoalId() {
        return historySoalId;
    }

    public void setHistorySoalId(String historySoalId) {
        this.historySoalId = historySoalId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getJenjang() {
        return jenjang;
    }

    public void setJenjang(String jenjang) {
        this.jenjang = jenjang;
    }

    public String getKelas() {
        return kelas;
    }

    public void setKelas(String kelas) {
        this.kelas = kelas;
    }

    public String getMatpel() {
        return matpel;
    }

    public void setMatpel(String matpel) {
        this.matpel = matpel;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public List<SoalResult> getSoalResultList() {
        return soalResultList;
    }

    public void setSoalResultList(List<SoalResult> soalResultList) {
        this.soalResultList = soalResultList;
    }

    public Integer getScore() {
        return score;
    }

    public void setScore(Integer score) {
        this.score = score;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }
}
