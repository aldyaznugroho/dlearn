package com.example.prasetio.dlearn.api;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Prasetio on 7/19/2017.
 */

public class ApiAdapter {
    private static Retrofit retrofit;

    public static ApiAdapter getInstance() {
        return new ApiAdapter();
    }

    public Retrofit getRetrofit(String urlEndpoint) {
        retrofit = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(urlEndpoint)
                .build();
        return retrofit;
    }
}
