package com.example.prasetio.dlearn.view.adapter;

import android.content.Context;
import android.support.annotation.IntRange;
import android.support.annotation.NonNull;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.prasetio.dlearn.R;
import com.example.prasetio.dlearn.model.JawabanSoalLog;
import com.example.prasetio.dlearn.model.Soal;
import com.example.prasetio.dlearn.utils.MainConst;
import com.example.prasetio.dlearn.view.widgets.ReviewSoalLayout;
import com.example.prasetio.dlearn.view.widgets.SoalLayout;
import com.stepstone.stepper.Step;
import com.stepstone.stepper.adapter.AbstractStepAdapter;
import com.stepstone.stepper.viewmodel.StepViewModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Aldyaz on 7/23/2017.
 */

public class SoalStepAdapter extends AbstractStepAdapter {

    private SparseArray<Step> pages = new SparseArray<>();
    private List<Soal> itemSoal;

    public SoalStepAdapter(@NonNull Context context) {
        super(context);
        itemSoal = new ArrayList<>();
    }

    @Override
    public Step createStep(@IntRange(from = 0L) int position) {
        return new SoalLayout(context, itemSoal.get(position), position);
    }

    @NonNull
    @Override
    public StepViewModel getViewModel(@IntRange(from = 0L) int position) {
        return super.getViewModel(position);
    }

    @Override
    public Step findStep(@IntRange(from = 0L) int position) {
        return (pages.size() > 0) ? pages.get(position) : null;
    }

    @Override
    public int getCount() {
        return (itemSoal == null) ? 0 : itemSoal.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        Step step = pages.get(position);
        if (step == null) {
            step = createStep(position);
            pages.put(position, step);
        }

        View stepView = (View) step;
        container.addView(stepView);

        return stepView;
    }

    public void addAllItem(List<Soal> soalList) {
        itemSoal.addAll(soalList);
        notifyDataSetChanged();
    }
}
