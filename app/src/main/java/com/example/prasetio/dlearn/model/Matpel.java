package com.example.prasetio.dlearn.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Aldyaz on 7/20/2017.
 */

public class Matpel {
    @SerializedName("id")
    @Expose
    private Integer matpelId;
    @SerializedName("nama_matpel")
    @Expose
    private String namaMatpel;

    public Integer getMatpelId() {
        return matpelId;
    }

    public void setMatpelId(Integer matpelId) {
        this.matpelId = matpelId;
    }

    public String getNamaMatpel() {
        return namaMatpel;
    }

    public void setNamaMatpel(String namaMatpel) {
        this.namaMatpel = namaMatpel;
    }
}
