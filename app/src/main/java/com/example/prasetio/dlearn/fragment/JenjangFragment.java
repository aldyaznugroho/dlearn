package com.example.prasetio.dlearn.fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.prasetio.dlearn.R;
import com.example.prasetio.dlearn.activity.KelasActivity;
import com.example.prasetio.dlearn.api.ApiAdapter;
import com.example.prasetio.dlearn.api.ApiService;
import com.example.prasetio.dlearn.model.Jenjang;
import com.example.prasetio.dlearn.model.JenjangData;
import com.example.prasetio.dlearn.utils.MainConst;
import com.example.prasetio.dlearn.view.adapter.CustomRecyclerAdapter;
import com.kennyc.view.MultiStateView;

import java.net.SocketTimeoutException;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;


/**
 * A simple {@link Fragment} subclass.
 */
public class JenjangFragment extends Fragment {

    @BindView(R.id.state_view) MultiStateView stateView;
    @BindView(R.id.rv_jenjang) RecyclerView recyclerView;

    private ApiService apiService;
    private CustomRecyclerAdapter<Jenjang> rvAdapter;

    public static JenjangFragment getInstance() {
        return new JenjangFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_jenjang, container, false);
        ButterKnife.bind(this, rootView);
        init();
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initRetrofit();
    }

    private void init() {
        initStateView();
        initRecyclerView();
    }

    private void initStateView() {
        stateView.getView(MultiStateView.VIEW_STATE_ERROR).findViewById(R.id.state_retry_button)
                .setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        callJenjangApi();
                    }
                });
    }

    private void initRecyclerView() {
        rvAdapter = new CustomRecyclerAdapter<>(getContext(), R.layout.item_category_level_holder, new CustomRecyclerAdapter.ViewHolderBinder<Jenjang>() {
            @Override
            public void bind(CustomRecyclerAdapter.CustomViewHolder<Jenjang> holder, Jenjang item) {
                CardView jenjangHolderBg = (CardView) holder.itemView.findViewById(R.id.jenjang_holder_bg);
                ImageView jenjangImage = (ImageView) holder.itemView.findViewById(R.id.iv_jenjang);

                final Integer jenjangId = item.getJenjangId();
                final String namaJenjang = item.getNamaJenjang();

                switch (namaJenjang) {
                    case "sd":
                        jenjangImage.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.jenjang_sd));
                        jenjangImage.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.color_red_700));
                        break;
                    case "smp":
                        jenjangImage.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.jenjang_smp));
                        jenjangImage.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.color_blue_800));
                        break;
                    case "sma/smk":
                        jenjangImage.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.jenjang_sma));
                        jenjangImage.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.color_indigo_500));
                        break;
                }

                jenjangHolderBg.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Bundle bundle = new Bundle();
                        bundle.putInt(MainConst.KEY_INTENT_JENJANG_ID, jenjangId);
                        bundle.putString(MainConst.KEY_INTENT_JENJANG_NAMA, namaJenjang);

                        Intent kelasIntent = new Intent(getContext(), KelasActivity.class);
                        kelasIntent.putExtras(bundle);
                        startActivity(kelasIntent);
                    }
                });
            }
        });

        recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(rvAdapter);
    }

    private void initRetrofit() {
        apiService = ApiAdapter.getInstance().getRetrofit(MainConst.BASE_URL).create(ApiService.class);
        callJenjangApi();
    }

    private void callJenjangApi() {
        stateView.setViewState(MultiStateView.VIEW_STATE_LOADING);
        Call<JenjangData> jenjangDataCall = apiService.getJenjang();
        jenjangDataCall.enqueue(new Callback<JenjangData>() {
            @Override
            public void onResponse(Call<JenjangData> call, Response<JenjangData> response) {
                List<Jenjang> items = response.body().getListJenjang();

                if (response.isSuccessful()) {
                    stateView.setViewState(MultiStateView.VIEW_STATE_CONTENT);
                    rvAdapter.addAll(items);
                } else {
                    Timber.e("MESSAGE: " + response.message());
                    Timber.e("RESPONSE CODE " + response.code());
                    Toast.makeText(getContext(), "Error response", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<JenjangData> call, Throwable t) {
                if (t instanceof SocketTimeoutException) {
                    Toast.makeText(getContext(), "Request timeout. Please try again", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getContext(), "Network connecting error", Toast.LENGTH_SHORT).show();
                }

                stateView.setViewState(MultiStateView.VIEW_STATE_ERROR);

                Timber.e("ERROR: " + t.getMessage());
            }
        });
    }

}
