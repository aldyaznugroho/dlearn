package com.example.prasetio.dlearn.view.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;

import com.example.prasetio.dlearn.model.OpsiJawaban;
import com.example.prasetio.dlearn.utils.MainConst;
import com.example.prasetio.dlearn.view.widgets.ChoiceItemViewTextOnly;
import com.example.prasetio.dlearn.view.widgets.ChoiceItemViewWithImage;
import com.github.stephenvinouze.advancedrecyclerview_core.adapters.RecyclerAdapter;

import org.jetbrains.annotations.NotNull;

/**
 * Created by Prasetio on 7/25/2017.
 */

public class OpsiSoalAdapter extends RecyclerAdapter<OpsiJawaban> {

    private boolean pictureIsAvailable = false;

    public OpsiSoalAdapter(Context context) {
        super(context);
    }

    @NotNull
    @Override
    protected View onCreateItemView(ViewGroup viewGroup, int i) {
        if (!checkPictureAvailability(i)) {
            pictureIsAvailable = true;
            return new ChoiceItemViewWithImage(getContext());
        } else {
            pictureIsAvailable = false;
            return new ChoiceItemViewTextOnly(getContext());
        }
    }

    @Override
    protected void onBindItemView(View view, int i) {
        if (view instanceof ChoiceItemViewTextOnly) {
            ChoiceItemViewTextOnly itemViewTextOnly = (ChoiceItemViewTextOnly) view;
            itemViewTextOnly.bind(getItems().get(i), i, isItemViewToggled(i));
        } else if (view instanceof ChoiceItemViewWithImage) {
            ChoiceItemViewWithImage itemViewWithImage = (ChoiceItemViewWithImage) view;
            itemViewWithImage.bind(getItems().get(i), i, isItemViewToggled(i));
        }
    }

    private boolean checkPictureAvailability(int position) {
        String picture = getItems().get(position).getPicture();
        return picture == null || picture.equalsIgnoreCase("");
    }

    public boolean isPictureIsAvailable() {
        return pictureIsAvailable;
    }
}
