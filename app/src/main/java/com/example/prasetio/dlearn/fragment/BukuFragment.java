package com.example.prasetio.dlearn.fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.prasetio.dlearn.R;
import com.example.prasetio.dlearn.activity.ViewBukuActivity;
import com.example.prasetio.dlearn.model.BukuModel;
import com.example.prasetio.dlearn.view.adapter.CustomRecyclerAdapter;
import com.example.prasetio.dlearn.view.widgets.DividerItemDecoration;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import timber.log.Timber;


/**
 * A simple {@link Fragment} subclass.
 */
public class BukuFragment extends Fragment {

    @BindView(R.id.rv_buku) RecyclerView recyclerView;

    private ArrayList<BukuModel> listBuku = new ArrayList<>();
    private CustomRecyclerAdapter<BukuModel> rvAdapter;

    public static BukuFragment getInstance() {
        return new BukuFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_buku, container, false);
        ButterKnife.bind(this, rootView);
        init();
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        rvAdapter.addAll(listBuku);
    }

    private void init() {
        initRecyclerView();

        addDataBuku();
    }

    private void initRecyclerView() {
        rvAdapter = new CustomRecyclerAdapter<>(getContext(), R.layout.item_buku_holder, new CustomRecyclerAdapter.ViewHolderBinder<BukuModel>() {
            @Override
            public void bind(CustomRecyclerAdapter.CustomViewHolder<BukuModel> holder, final BukuModel item) {
                CardView jenjangHolderBg = (CardView) holder.itemView.findViewById(R.id.buku_holder_bg);
                TextView matpelText = (TextView) holder.itemView.findViewById(R.id.tv_matpel);
                TextView jenjangText = (TextView) holder.itemView.findViewById(R.id.tv_jenjang);
                TextView kelasText = (TextView) holder.itemView.findViewById(R.id.tv_kelas);

                matpelText.setText(item.getMatpel());
                jenjangText.setText(item.getJenjang());
                kelasText.setText(item.getKelas());

                jenjangHolderBg.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Bundle bundle = new Bundle();
                        bundle.putParcelable("key_buku", item);

                        Intent viewBukuIntent = new Intent(getContext(), ViewBukuActivity.class);
                        viewBukuIntent.putExtras(bundle);
                        getContext().startActivity(viewBukuIntent);
                    }
                });
            }
        });

        recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        recyclerView.addItemDecoration(new DividerItemDecoration(getContext(), LinearLayoutManager.VERTICAL));
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(rvAdapter);
    }

    private void addDataBuku() {
        BukuModel ipaSmpKelas7 = new BukuModel();
        ipaSmpKelas7.setJenjang("SMP");
        ipaSmpKelas7.setKelas("Kelas 7");
        ipaSmpKelas7.setMatpel("IPA");
        ipaSmpKelas7.setNamaFile("ipa-smp-kelas-7.pdf");

        BukuModel ipaSmpKelas8 = new BukuModel();
        ipaSmpKelas8.setJenjang("SMP");
        ipaSmpKelas8.setKelas("Kelas 8");
        ipaSmpKelas8.setMatpel("IPA");
        ipaSmpKelas8.setNamaFile("ipa-smp-kelas-8.pdf");

        BukuModel ipaSmpKelas9 = new BukuModel();
        ipaSmpKelas9.setJenjang("SMP");
        ipaSmpKelas9.setKelas("Kelas 9");
        ipaSmpKelas9.setMatpel("IPA");
        ipaSmpKelas9.setNamaFile("ipa-smp-kelas-9.pdf");

        listBuku.add(ipaSmpKelas7);
        listBuku.add(ipaSmpKelas8);
        listBuku.add(ipaSmpKelas9);
    }

}
