package com.example.prasetio.dlearn;

import android.app.Application;
import android.content.Context;

import com.example.prasetio.dlearn.db.DlearnDb;
import com.example.prasetio.dlearn.model.Jenjang;
import com.raizlabs.android.dbflow.config.FlowConfig;
import com.raizlabs.android.dbflow.config.FlowManager;

import java.util.ArrayList;
import java.util.List;

import timber.log.Timber;

/**
 * Created by Prasetio on 3/7/2017.
 */
public class BaseApp extends Application {

    public static BaseApp get(Context context) {
        return (BaseApp) context.getApplicationContext();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
        }
    }
}
