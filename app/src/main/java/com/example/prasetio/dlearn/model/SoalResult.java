package com.example.prasetio.dlearn.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Aldyaz on 8/2/2017.
 */

public class SoalResult {
    @SerializedName("jawaban_dipilih")
    @Expose
    private String jawabanDipilih;
    @SerializedName("jawaban_benar")
    @Expose
    private String jawabanBenar;
    @SerializedName("soal")
    @Expose
    private String soal;
    @SerializedName("picture")
    @Expose
    private String picture;
    @SerializedName("pembahasan_soal")
    @Expose
    private String pembahasanSoal;

    public String getJawabanDipilih() {
        return jawabanDipilih;
    }

    public void setJawabanDipilih(String jawabanDipilih) {
        this.jawabanDipilih = jawabanDipilih;
    }

    public String getJawabanBenar() {
        return jawabanBenar;
    }

    public void setJawabanBenar(String jawabanBenar) {
        this.jawabanBenar = jawabanBenar;
    }

    public String getSoal() {
        return soal;
    }

    public void setSoal(String soal) {
        this.soal = soal;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getPembahasanSoal() {
        return pembahasanSoal;
    }

    public void setPembahasanSoal(String pembahasanSoal) {
        this.pembahasanSoal = pembahasanSoal;
    }
}
