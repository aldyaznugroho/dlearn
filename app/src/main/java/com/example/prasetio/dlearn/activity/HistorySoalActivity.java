package com.example.prasetio.dlearn.activity;

import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.net.ParseException;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.example.prasetio.dlearn.R;
import com.example.prasetio.dlearn.model.HistorySoal;
import com.example.prasetio.dlearn.utils.MainConst;
import com.example.prasetio.dlearn.utils.SharedPrefCustom;
import com.example.prasetio.dlearn.view.adapter.CustomRecyclerAdapter;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.kennyc.view.MultiStateView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import timber.log.Timber;

public class HistorySoalActivity extends AppCompatActivity
        implements ChildEventListener, ValueEventListener {

    @BindView(R.id.history_soal_toolbar) Toolbar toolbar;
    @BindView(R.id.history_soal_state_view) MultiStateView stateView;
    @BindView(R.id.history_soal_recycler_view) RecyclerView recyclerView;

    private DatabaseReference historyDbRef;
    private CustomRecyclerAdapter<HistorySoal> rvAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history_soal);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        init();
        setTitle("History");
    }

    @Override
    protected void onStart() {
        super.onStart();
        historyDbRef.addListenerForSingleValueEvent(this);
    }

    private void init() {
        initToolbar();
        initStateView();
        initRecyclerView();
        initFirebase();
    }

    private void initToolbar() {
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        Drawable navigationIcon = toolbar.getNavigationIcon();
        Drawable overflowIcon = toolbar.getOverflowIcon();
        if (overflowIcon != null && navigationIcon != null) {
            navigationIcon.mutate();
            navigationIcon.setColorFilter(
                    ContextCompat.getColor(this, R.color.color_white),
                    PorterDuff.Mode.SRC_ATOP);

            overflowIcon.mutate();
            overflowIcon.setColorFilter(
                    ContextCompat.getColor(this, R.color.color_white),
                    PorterDuff.Mode.SRC_ATOP);
        }
    }

    private void initStateView() {
        stateView.setViewState(MultiStateView.VIEW_STATE_LOADING);
        stateView.getView(MultiStateView.VIEW_STATE_ERROR).findViewById(R.id.state_retry_button)
                .setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        callHistoryListApi();
                    }
                });
    }

    private void initRecyclerView() {
        rvAdapter = new CustomRecyclerAdapter<>(this, R.layout.item_history_holder, new CustomRecyclerAdapter.ViewHolderBinder<HistorySoal>() {
            @Override
            public void bind(CustomRecyclerAdapter.CustomViewHolder<HistorySoal> holder, final HistorySoal item) {
                CardView historyCardView = (CardView) holder.itemView.findViewById(R.id.cardview_history);
                TextView historyJenjangText = (TextView) holder.itemView.findViewById(R.id.history_jenjang);
                TextView historyKelasText = (TextView) holder.itemView.findViewById(R.id.history_kelas);
                TextView historyMatpelText = (TextView) holder.itemView.findViewById(R.id.history_matpel);
                TextView historyLevelText = (TextView) holder.itemView.findViewById(R.id.history_level);
                TextView historyDateText = (TextView) holder.itemView.findViewById(R.id.history_date);
                TextView historyScoreText = (TextView) holder.itemView.findViewById(R.id.history_score);
                View historyJenjangColor = holder.itemView.findViewById(R.id.jenjang_color);

                final String historyId = item.getHistorySoalId();
                final String jenjang = item.getJenjang();
                final String kelas = item.getKelas();
                final String matpel = item.getMatpel();
                final String level = item.getLevel();
                final Integer score = item.getScore();

                historyJenjangText.setText(jenjang);
                historyKelasText.setText(kelas);
                historyMatpelText.setText(matpel);
                historyLevelText.setText(level);
                historyScoreText.setText(String.valueOf(score));

                switch (jenjang) {
                    case "sd":
                        historyJenjangColor.setBackgroundColor(ContextCompat.getColor(HistorySoalActivity.this, R.color.color_red_700));
                        break;

                    case "smp":
                        historyJenjangColor.setBackgroundColor(ContextCompat.getColor(HistorySoalActivity.this, R.color.color_blue_800));
                        break;

                    case "sma/smk":
                        historyJenjangColor.setBackgroundColor(ContextCompat.getColor(HistorySoalActivity.this, R.color.color_indigo_500));
                        break;
                }

                try {
                    DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss a", Locale.getDefault());
                    Date baseDate = dateFormat.parse(item.getTimestamp());
                    DateFormat dateFormatPengerjaan = new SimpleDateFormat("dd-MMM-yyyy", Locale.getDefault());
                    DateFormat timeFormatPengerjaan = new SimpleDateFormat("h:mm", Locale.getDefault());

                    historyDateText.setText(dateFormatPengerjaan.format(baseDate) + " - " + timeFormatPengerjaan.format(baseDate));
                } catch (java.text.ParseException e) {
                    e.printStackTrace();
                }

                historyCardView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Bundle bundle = new Bundle();
                        bundle.putInt(MainConst.KEY_REVIEW_SOAL_SCORE, score);
                        bundle.putString(MainConst.KEY_REVIEW_FIREBASE_RANDOM_KEY, historyId);
                        bundle.putString(MainConst.KEY_REVIEW_SOAL_JENJANG, jenjang);
                        bundle.putString(MainConst.KEY_REVIEW_SOAL_KELAS, kelas);
                        bundle.putString(MainConst.KEY_REVIEW_SOAL_MATPEL, matpel);
                        bundle.putString(MainConst.KEY_REVIEW_SOAL_LEVEL, level);

                        Intent soalResultIntent = new Intent(HistorySoalActivity.this, SoalResultActivity.class);
                        soalResultIntent.putExtras(bundle);
                        startActivity(soalResultIntent);
                    }
                });

            }
        });

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        linearLayoutManager.setReverseLayout(true);
        linearLayoutManager.setStackFromEnd(true);

        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(rvAdapter);
    }

    private void initFirebase() {
        Integer userId = SharedPrefCustom.getInstance(this).getSharedPrefUser().getUserId();
        historyDbRef = FirebaseDatabase.getInstance()
                .getReference(MainConst.KEY_DB_SOAL_HISTORY)
                .child("" + userId);
    }

    private void callHistoryListApi() {
        historyDbRef.orderByChild("timestamp").addChildEventListener(this);
    }

    @Override
    public void onChildAdded(DataSnapshot dataSnapshot, String s) {
        if (dataSnapshot.exists() && dataSnapshot.getValue() != null) {
            stateView.setViewState(MultiStateView.VIEW_STATE_CONTENT);

            try {
                HistorySoal historySoal = dataSnapshot.getValue(HistorySoal.class);
                if (historySoal != null) {
                    rvAdapter.add(historySoal);
                } else {
                    stateView.setViewState(MultiStateView.VIEW_STATE_EMPTY);
                }
            } catch (Exception e) {
                Timber.e("Error: " + e.getMessage());
                stateView.setViewState(MultiStateView.VIEW_STATE_ERROR);
            }
        }
    }

    @Override
    public void onChildChanged(DataSnapshot dataSnapshot, String s) {
        Timber.d("onChildChanged");
    }

    @Override
    public void onChildRemoved(DataSnapshot dataSnapshot) {
        Timber.d("onChildRemoved");
    }

    @Override
    public void onChildMoved(DataSnapshot dataSnapshot, String s) {
        Timber.d("onChildMoved");
    }

    @Override
    public void onDataChange(DataSnapshot dataSnapshot) {
        if (dataSnapshot.getChildrenCount() > 0) {
            callHistoryListApi();
        } else {
            stateView.setViewState(MultiStateView.VIEW_STATE_EMPTY);
        }
    }

    @Override
    public void onCancelled(DatabaseError databaseError) {
        Timber.d("onCancelled");
    }
}
