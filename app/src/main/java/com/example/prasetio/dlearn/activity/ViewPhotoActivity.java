package com.example.prasetio.dlearn.activity;

import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.example.prasetio.dlearn.R;
import com.example.prasetio.dlearn.utils.MainConst;
import com.github.chrisbanes.photoview.PhotoView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ViewPhotoActivity extends AppCompatActivity {

    @BindView(R.id.view_photo_toolbar) Toolbar toolbar;
    @BindView(R.id.soal_photo_view) PhotoView photoView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_photo);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        init();
        setTitle(null);
    }

    private void init() {
        initToolbar();
        initPhotoView();
    }

    private void initToolbar() {
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        Drawable navigationIcon = toolbar.getNavigationIcon();
        Drawable overflowIcon = toolbar.getOverflowIcon();
        if (overflowIcon != null && navigationIcon != null) {
            navigationIcon.mutate();
            navigationIcon.setColorFilter(
                    ContextCompat.getColor(this, R.color.color_white),
                    PorterDuff.Mode.SRC_ATOP);

            overflowIcon.mutate();
            overflowIcon.setColorFilter(
                    ContextCompat.getColor(this, R.color.color_white),
                    PorterDuff.Mode.SRC_ATOP);
        }
    }

    private void initPhotoView() {
        String image = getIntent().getExtras().getString(MainConst.KEY_INTENT_SOAL_VIEW_PHOTO);
        Glide.with(this)
                .load(image)
                .skipMemoryCache(true)
                .dontAnimate()
                .into(photoView);
    }

}
