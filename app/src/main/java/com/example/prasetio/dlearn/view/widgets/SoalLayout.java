package com.example.prasetio.dlearn.view.widgets;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.AttrRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StyleRes;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.example.prasetio.dlearn.R;
import com.example.prasetio.dlearn.activity.ViewPhotoActivity;
import com.example.prasetio.dlearn.model.JawabanSoalLog;
import com.example.prasetio.dlearn.model.OpsiJawaban;
import com.example.prasetio.dlearn.model.Soal;
import com.example.prasetio.dlearn.utils.MainConst;
import com.example.prasetio.dlearn.utils.SharedPrefCustom;
import com.example.prasetio.dlearn.view.adapter.OpsiSoalAdapter;
import com.github.stephenvinouze.advancedrecyclerview_core.adapters.RecyclerAdapter;
import com.github.stephenvinouze.advancedrecyclerview_core.callbacks.ClickCallback;
import com.stepstone.stepper.BlockingStep;
import com.stepstone.stepper.StepperLayout;
import com.stepstone.stepper.VerificationError;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Prasetio on 7/23/2017.
 */

public class SoalLayout extends LinearLayout implements BlockingStep {

    @BindView(R.id.soal_case_tv) TextView soalCaseText;
    @BindView(R.id.soal_case_iv) CircleImageView soalCaseIv;
    @BindView(R.id.opsi_rv) RecyclerView opsiRecyclerView;

    private Soal soal;
    private OpsiJawaban opsi;
    private int soalPosition;
    private OpsiSoalAdapter opsiAdapter;

    public SoalLayout(@NonNull Context context, Soal soal, int position) {
        super(context);
        this.soal = soal;
        this.soalPosition = position;
        initViews();
    }

    public SoalLayout(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public SoalLayout(@NonNull Context context, @Nullable AttributeSet attrs, @AttrRes int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public SoalLayout(@NonNull Context context, @Nullable AttributeSet attrs, @AttrRes int defStyleAttr, @StyleRes int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    private void initViews() {
        View view = inflate(getContext(), R.layout.layout_soal, this);
        ButterKnife.bind(this, view);
        bind();
    }

    private void bind() {
        if (soal != null) {
            Integer nomorSoal = soalPosition + 1;
            String nomorSoalFormat = "Nomor " + nomorSoal;

            soalCaseText.setText(soal.getSoal());

            if (soal.getPicture() == null || soal.getPicture().equals("")) {
                soalCaseIv.setVisibility(GONE);
            } else {
                Glide.with(getContext())
                        .load(soal.getPicture())
                        .diskCacheStrategy(DiskCacheStrategy.RESULT)
                        .skipMemoryCache(true)
                        .dontAnimate()
                        .into(soalCaseIv);

                soalCaseIv.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (soalCaseIv != null) {
                            Bundle bundle = new Bundle();
                            bundle.putString(MainConst.KEY_INTENT_SOAL_VIEW_PHOTO, soal.getPicture());

                            Intent viewPhotoIntent = new Intent(getContext(), ViewPhotoActivity.class);
                            viewPhotoIntent.putExtras(bundle);
                            getContext().startActivity(viewPhotoIntent);
                        }
                    }
                });
            }

            opsiAdapter = new OpsiSoalAdapter(getContext());
            opsiAdapter.setItems(soal.getOpsiJawaban());
            opsiAdapter.setChoiceMode(RecyclerAdapter.ChoiceMode.SINGLE_CHOICE);
            opsiAdapter.setClickCallback(new ClickCallback() {
                @Override
                public void onItemClick(View view, int i) {
                    opsi = opsiAdapter.getItems().get(i);

                    JawabanSoalLog jawabanSoalLog = new JawabanSoalLog();
                    jawabanSoalLog.setSoal_id(soal.getSoalId());
                    jawabanSoalLog.setJawaban_dipilih(opsi.getText());
                    jawabanSoalLog.setJawaban_benar(soal.getJawabanBenar());

                    SharedPrefCustom.getInstance(getContext())
                            .putSharedPrefJawabanSoalLog(jawabanSoalLog, soalPosition);
                }
            });

            opsiRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
            opsiRecyclerView.setNestedScrollingEnabled(false);
            opsiRecyclerView.setHasFixedSize(true);
            opsiRecyclerView.setItemAnimator(null);
            opsiRecyclerView.setAdapter(opsiAdapter);
        }
    }

    private int scoring(String jawabanDipilih) {
        if (jawabanDipilih == null) {
            return 0;
        } else {
            if (jawabanDipilih.equalsIgnoreCase(soal.getJawabanBenar())) {
                return 5;
            } else {
                return 0;
            }
        }
    }

    @Override
    public VerificationError verifyStep() {
        return null;
    }

    @Override
    public void onSelected() {
    }

    @Override
    public void onError(@NonNull VerificationError error) {
    }

    @Override
    public void onNextClicked(StepperLayout.OnNextClickedCallback callback) {
        callback.goToNextStep();
    }

    @Override
    public void onCompleteClicked(StepperLayout.OnCompleteClickedCallback callback) {
        callback.complete();
    }

    @Override
    public void onBackClicked(StepperLayout.OnBackClickedCallback callback) {
        callback.goToPrevStep();
    }
}
