package com.example.prasetio.dlearn.activity;

import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.prasetio.dlearn.R;
import com.example.prasetio.dlearn.api.ApiAdapter;
import com.example.prasetio.dlearn.api.ApiService;
import com.example.prasetio.dlearn.model.Tingkatan;
import com.example.prasetio.dlearn.model.TingkatanData;
import com.example.prasetio.dlearn.utils.MainConst;
import com.example.prasetio.dlearn.view.adapter.CustomRecyclerAdapter;
import com.kennyc.view.MultiStateView;

import java.net.SocketTimeoutException;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

public class LevelActivity extends AppCompatActivity {

    @BindView(R.id.level_toolbar) Toolbar toolbar;
    @BindView(R.id.level_state_layout) MultiStateView stateView;
    @BindView(R.id.rv_level) RecyclerView recyclerView;

    private ApiService apiService;
    private CustomRecyclerAdapter<Tingkatan> rvAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_level);
        ButterKnife.bind(this);
        init();
        setTitle(getIntent().getExtras().getString(MainConst.KEY_INTENT_KELAS_NAMA));
    }

    private void init() {
        initToolbar();
        initStateView();
        initRecyclerView();
        initRetrofit();
    }

    private void initToolbar() {
        toolbar.setTitle("Pilih Level");
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                backToMatpelActivity();
            }
        });

        Drawable navigationIcon = toolbar.getNavigationIcon();
        Drawable overflowIcon = toolbar.getOverflowIcon();
        if (overflowIcon != null && navigationIcon != null) {
            navigationIcon.mutate();
            navigationIcon.setColorFilter(
                    ContextCompat.getColor(this, R.color.color_white),
                    PorterDuff.Mode.SRC_ATOP);

            overflowIcon.mutate();
            overflowIcon.setColorFilter(
                    ContextCompat.getColor(this, R.color.color_white),
                    PorterDuff.Mode.SRC_ATOP);
        }
    }

    private void initStateView() {
        stateView.getView(MultiStateView.VIEW_STATE_ERROR).findViewById(R.id.state_retry_button)
                .setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        callLevelApi();
                    }
                });
    }

    private void initRecyclerView() {
        rvAdapter = new CustomRecyclerAdapter<>(this, R.layout.item_level_holder, new CustomRecyclerAdapter.ViewHolderBinder<Tingkatan>() {
            @Override
            public void bind(CustomRecyclerAdapter.CustomViewHolder<Tingkatan> holder, final Tingkatan item) {
                CardView levelCardBg = (CardView) holder.itemView.findViewById(R.id.cardview_level);
                TextView levelTextView = (TextView) holder.itemView.findViewById(R.id.tv_level);

                final Integer levelId = item.getLevelId();

                levelTextView.setText(item.getNamaLevel());
                levelCardBg.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Bundle bundle = new Bundle();
                        bundle.putInt(MainConst.KEY_INTENT_JENJANG_ID,
                                getIntent().getExtras().getInt(MainConst.KEY_INTENT_JENJANG_ID));
                        bundle.putInt(MainConst.KEY_INTENT_KELAS_ID,
                                getIntent().getExtras().getInt(MainConst.KEY_INTENT_KELAS_ID));
                        bundle.putInt(MainConst.KEY_INTENT_MATPEL_ID,
                                getIntent().getExtras().getInt(MainConst.KEY_INTENT_MATPEL_ID));
                        bundle.putInt(MainConst.KEY_INTENT_LEVEL_ID, levelId);

                        bundle.putString(MainConst.KEY_REVIEW_SOAL_JENJANG, getIntent().getExtras().getString(MainConst.KEY_INTENT_JENJANG_NAMA));
                        bundle.putString(MainConst.KEY_REVIEW_SOAL_KELAS, getIntent().getExtras().getString(MainConst.KEY_INTENT_KELAS_NAMA));
                        bundle.putString(MainConst.KEY_REVIEW_SOAL_MATPEL, getIntent().getExtras().getString(MainConst.KEY_INTENT_MATPEL_NAMA));
                        bundle.putString(MainConst.KEY_REVIEW_SOAL_LEVEL, item.getNamaLevel());

                        Intent soalIntent = new Intent(LevelActivity.this, SoalActivity.class);
                        soalIntent.putExtras(bundle);
                        startActivity(soalIntent);
                        finish();
                    }
                });
            }
        });

        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(rvAdapter);
    }

    private void initRetrofit() {
        apiService = ApiAdapter.getInstance().getRetrofit(MainConst.BASE_URL).create(ApiService.class);
        callLevelApi();
    }

    private void callLevelApi() {
        stateView.setViewState(MultiStateView.VIEW_STATE_LOADING);
        Call<TingkatanData> tingkatanDataCall = apiService.getLevel();
        tingkatanDataCall.enqueue(new Callback<TingkatanData>() {
            @Override
            public void onResponse(Call<TingkatanData> call, Response<TingkatanData> response) {
                List<Tingkatan> items = response.body().getListTingkatan();

                if (response.isSuccessful()) {
                    stateView.setViewState(MultiStateView.VIEW_STATE_CONTENT);
                    rvAdapter.addAll(items);
                } else {
                    Timber.e("MESSAGE: " + response.message());
                    Timber.e("RESPONSE CODE " + response.code());
                    Toast.makeText(LevelActivity.this, "Error response", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<TingkatanData> call, Throwable t) {
                if (t instanceof SocketTimeoutException) {
                    Toast.makeText(LevelActivity.this, "Request timeout. Please try again", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getApplicationContext(), "Network connecting error", Toast.LENGTH_SHORT).show();
                }

                stateView.setViewState(MultiStateView.VIEW_STATE_ERROR);

                Timber.e("ERROR: " + t.getMessage());
            }
        });
    }

    private void backToMatpelActivity() {
        Bundle bundle = new Bundle();
        bundle.putInt(MainConst.KEY_INTENT_JENJANG_ID, getIntent().getExtras().getInt(MainConst.KEY_INTENT_JENJANG_ID));
        bundle.putInt(MainConst.KEY_INTENT_KELAS_ID, getIntent().getExtras().getInt(MainConst.KEY_INTENT_KELAS_ID));
        bundle.putString(MainConst.KEY_INTENT_KELAS_NAMA, getIntent().getExtras().getString(MainConst.KEY_INTENT_KELAS_NAMA));
        bundle.putString(MainConst.KEY_INTENT_JENJANG_NAMA, getIntent().getExtras().getString(MainConst.KEY_INTENT_JENJANG_NAMA));

        Intent kelasIntent = new Intent(LevelActivity.this, MatpelActivity.class);
        kelasIntent.putExtras(bundle);
        startActivity(kelasIntent);
        finish();
    }

    @Override
    public void onBackPressed() {
        backToMatpelActivity();
    }
}
