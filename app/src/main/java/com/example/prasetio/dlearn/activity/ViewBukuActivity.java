package com.example.prasetio.dlearn.activity;

import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.example.prasetio.dlearn.R;
import com.example.prasetio.dlearn.model.BukuModel;
import com.github.barteksc.pdfviewer.PDFView;
import com.github.barteksc.pdfviewer.scroll.DefaultScrollHandle;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ViewBukuActivity extends AppCompatActivity {

    private BukuModel bukuModel;

    @BindView(R.id.view_buku_toolbar) Toolbar toolbar;
    @BindView(R.id.pdf_view) PDFView pdfView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_buku);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        init();
        setTitle(bukuModel.getMatpel());
    }

    private void init() {
        bukuModel = getIntent().getParcelableExtra("key_buku");
        initToolbar();
        initPdfView();
    }

    private void initToolbar() {
        toolbar.setSubtitle(bukuModel.getJenjang() + " - " + bukuModel.getKelas());
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        Drawable navigationIcon = toolbar.getNavigationIcon();
        Drawable overflowIcon = toolbar.getOverflowIcon();
        if (overflowIcon != null && navigationIcon != null) {
            navigationIcon.mutate();
            navigationIcon.setColorFilter(
                    ContextCompat.getColor(this, R.color.color_white),
                    PorterDuff.Mode.SRC_ATOP);

            overflowIcon.mutate();
            overflowIcon.setColorFilter(
                    ContextCompat.getColor(this, R.color.color_white),
                    PorterDuff.Mode.SRC_ATOP);
        }
    }

    private void initPdfView() {
        pdfView.fromAsset(bukuModel.getNamaFile())
                .defaultPage(0)
                .enableSwipe(true)
                .scrollHandle(new DefaultScrollHandle(this))
                .password(null)
                .load();
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}
