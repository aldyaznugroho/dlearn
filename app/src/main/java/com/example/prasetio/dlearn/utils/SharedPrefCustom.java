package com.example.prasetio.dlearn.utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.example.prasetio.dlearn.model.JawabanSoalLog;
import com.example.prasetio.dlearn.model.OpsiJawaban;
import com.example.prasetio.dlearn.model.Soal;
import com.example.prasetio.dlearn.model.User;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;

/**
 * Created by Prasetio on 7/9/2017.
 */

public class SharedPrefCustom {
    public static final String PREFS_NAME = "SHARED_PREF";

    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;
    private static SharedPrefCustom instance = null;

    private SharedPrefCustom(Context context) {
        this.sharedPreferences = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
    }

    public static SharedPrefCustom getInstance(Context context) {
        if(instance == null)
            instance = new SharedPrefCustom(context);
        return instance;
    }

    public void putSharedPref(String key, String value) {
        this.sharedPreferences.edit().putString(key, value).apply();
    }

    public void putSharedPrefInt(String key, int value) {
        this.sharedPreferences.edit().putInt(key, value).apply();
    }


    public void putSharedPrefLong(String key, long value) {
        this.sharedPreferences.edit().putLong(key, value).apply();
    }


    public void putSharedPrefBoolean(String key, Boolean value) {
        this.sharedPreferences.edit().putBoolean(key, value).apply();
    }

    public void putSharedPrefStringArray(String key, ArrayList<String> values) {
        for(int i  = 0; i < values.size(); i++){
            putSharedPref(key + "_" + Integer.toString(i), values.get(i));
        }
        putSharedPrefInt(key + "_size", values.size());
    }

    public String getSharedPref(String key) {
        return this.sharedPreferences.getString(key, "");
    }

    public void removeSharedPref(String key) {
        this.sharedPreferences.edit().remove(key).apply();
    }

    public int getSharedPrefInt(String key) {
        return this.sharedPreferences.getInt(key, 0);
    }

    public Long getSharedPrefLong(String key) {
        return this.sharedPreferences.getLong(key, 0);
    }

    public Boolean getSharedPrefBoolean(String key) {
        return this.sharedPreferences.getBoolean(key, false);
    }

    public Boolean getSharedPrefBoolean(String key, boolean defaultBoolean) {
        return this.sharedPreferences.getBoolean(key, defaultBoolean);
    }

    public ArrayList<String> getSharedPrefStringArray(String key) {
        ArrayList<String> values = new ArrayList<>();
        for(int i = 0; i < getSharedPrefInt(key + "_size"); i++){
            values.add(getSharedPref(key + "_" + Integer.toString(i)));
        }
        return values;
    }

    public void clearSharedPrefs(){
        this.sharedPreferences.edit().clear().apply();
    }

    public void removeSharedPrefByKey(String key) {
        this.sharedPreferences.edit().remove(key).apply();
    }

    public boolean containsKey(String key){
        return this.sharedPreferences.contains(key);
    }

    public void putSharedPrefUser(User user) {
        editor = sharedPreferences.edit();
        editor.putInt(MainConst.KEY_USER_ID, user.getUserId());
        editor.putString(MainConst.KEY_USER_NAME, user.getNama());
        editor.putString(MainConst.KEY_USER_EMAIL, user.getEmail());
        editor.putString(MainConst.KEY_USER_PROFILE_IMAGE, user.getProfilePicture());
        editor.apply();
    }

    public User getSharedPrefUser() {
        Integer userId = sharedPreferences.getInt(MainConst.KEY_USER_ID, 0);
        String userName = sharedPreferences.getString(MainConst.KEY_USER_NAME, "");
        String userEmail = sharedPreferences.getString(MainConst.KEY_USER_EMAIL, "");
        String userProfileImage = sharedPreferences.getString(MainConst.KEY_USER_PROFILE_IMAGE, "");
        return new User(userId, userName, userEmail, userProfileImage);
    }

    public void removeSharedPrefUser() {
        editor = sharedPreferences.edit();
        editor.remove(MainConst.KEY_USER_ID).apply();
        editor.remove(MainConst.KEY_USER_NAME).apply();
        editor.remove(MainConst.KEY_USER_EMAIL).apply();
        editor.remove(MainConst.KEY_USER_PROFILE_IMAGE).apply();
        editor.apply();
    }

    public void putSharedPrefJawabanSoalLog(JawabanSoalLog jawabanSoalLog, int soalPosition) {
        editor = sharedPreferences.edit();
        editor.putInt(MainConst.KEY_JAWABAN_LOG_SOAL_ID + soalPosition, jawabanSoalLog.getSoal_id());
        editor.putString(MainConst.KEY_JAWABAN_LOG_DIPILIH + soalPosition, jawabanSoalLog.getJawaban_dipilih());
        editor.putString(MainConst.KEY_JAWABAN_LOG_BENAR + soalPosition, jawabanSoalLog.getJawaban_benar());
        editor.apply();
    }

    public JawabanSoalLog getSharedPrefJawabanSoalLog(int soalPosition) {
        Integer soalId = sharedPreferences.getInt(MainConst.KEY_JAWABAN_LOG_SOAL_ID + soalPosition, 0);
        String jawabanDipilih = sharedPreferences.getString(MainConst.KEY_JAWABAN_LOG_DIPILIH + soalPosition, null);
        String jawabanBenar = sharedPreferences.getString(MainConst.KEY_JAWABAN_LOG_BENAR + soalPosition, null);
        return new JawabanSoalLog(soalId, jawabanDipilih, jawabanBenar);
    }

    public void removeSharedPrefJawabanSoalLog(int soalPosition) {
        editor = sharedPreferences.edit();
        editor.remove(MainConst.KEY_JAWABAN_LOG_SOAL_ID + soalPosition).apply();
        editor.remove(MainConst.KEY_JAWABAN_LOG_DIPILIH + soalPosition).apply();
        editor.remove(MainConst.KEY_JAWABAN_LOG_BENAR + soalPosition).apply();
        editor.remove(MainConst.KEY_JAWABAN_LOG_NILAI + soalPosition).apply();
        editor.apply();
    }

    /**
     *
     *  =============================================================
     *
     */

    /*public void putTestJawabanSoalLog(JawabanSoalLog jawabanSoalLog) {
        ArrayList<JawabanSoalLog> jawabanSoalLogs = getTestJawabanSoalLog();
        if (jawabanSoalLogs == null) jawabanSoalLogs = new ArrayList<>();
        jawabanSoalLogs.add(jawabanSoalLog);
        saveTestJawabanSoalLog(jawabanSoalLogs);
    }

    public ArrayList<JawabanSoalLog> getTestJawabanSoalLog() {
        Gson gson = new Gson();
        String jsonList = sharedPreferences.getString(MainConst.KEY_JAWABAN_LOG_LIST, "");
        Type type = new TypeToken<ArrayList<JawabanSoalLog>>(){}.getType();
        return gson.fromJson(jsonList, type);
    }

    public void removeTestSoalLog(JawabanSoalLog jawabanSoalLog) {
        ArrayList<JawabanSoalLog> jawabanSoalLogs = getTestJawabanSoalLog();
        if (jawabanSoalLogs != null) {
            jawabanSoalLogs.remove(jawabanSoalLog);
            saveTestJawabanSoalLog(jawabanSoalLogs);
        }
    }

    public void removeTestAllSoalLog() {
        editor = sharedPreferences.edit();
        editor.remove(MainConst.KEY_JAWABAN_LOG_LIST).apply();
        editor.apply();
    }

    public void saveTestJawabanSoalLog(ArrayList<JawabanSoalLog> jawabanSoalLogs) {
        editor = sharedPreferences.edit();
        Gson gson = new Gson();
        String jsonList = gson.toJson(jawabanSoalLogs);
        editor.putString(MainConst.KEY_JAWABAN_LOG_LIST, jsonList);
        editor.apply();
    }*/

}
