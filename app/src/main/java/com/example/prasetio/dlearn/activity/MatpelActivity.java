package com.example.prasetio.dlearn.activity;

import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.prasetio.dlearn.R;
import com.example.prasetio.dlearn.api.ApiAdapter;
import com.example.prasetio.dlearn.api.ApiService;
import com.example.prasetio.dlearn.model.Matpel;
import com.example.prasetio.dlearn.model.MatpelData;
import com.example.prasetio.dlearn.utils.MainConst;
import com.example.prasetio.dlearn.view.adapter.CustomRecyclerAdapter;
import com.kennyc.view.MultiStateView;

import java.net.SocketTimeoutException;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

public class MatpelActivity extends AppCompatActivity {

    @BindView(R.id.matpel_toolbar) Toolbar toolbar;
    @BindView(R.id.matpel_state_layout) MultiStateView stateView;
    @BindView(R.id.rv_matpel) RecyclerView recyclerView;

    private ApiService apiService;
    private CustomRecyclerAdapter<Matpel> rvAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_matpel);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        init();
        setTitle(String.valueOf(getIntent().getExtras().getString(MainConst.KEY_INTENT_JENJANG_NAMA) + " - " +
                getIntent().getExtras().getString(MainConst.KEY_INTENT_KELAS_NAMA)).toUpperCase());
    }

    private void init() {
        initToolbar();
        initStateView();
        initRecyclerView();
        initRetrofit();
    }

    private void initToolbar() {
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                backToKelasActivity();
            }
        });

        Drawable navigationIcon = toolbar.getNavigationIcon();
        Drawable overflowIcon = toolbar.getOverflowIcon();
        if (overflowIcon != null && navigationIcon != null) {
            navigationIcon.mutate();
            navigationIcon.setColorFilter(
                    ContextCompat.getColor(this, R.color.color_white),
                    PorterDuff.Mode.SRC_ATOP);

            overflowIcon.mutate();
            overflowIcon.setColorFilter(
                    ContextCompat.getColor(this, R.color.color_white),
                    PorterDuff.Mode.SRC_ATOP);
        }
    }

    private void initStateView() {
        stateView.getView(MultiStateView.VIEW_STATE_ERROR).findViewById(R.id.state_retry_button)
                .setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        callMatpelApi();
                    }
                });
    }

    private void initRecyclerView() {
        rvAdapter = new CustomRecyclerAdapter<>(this, R.layout.item_matpel_holder, new CustomRecyclerAdapter.ViewHolderBinder<Matpel>() {
            @Override
            public void bind(CustomRecyclerAdapter.CustomViewHolder<Matpel> holder, final Matpel item) {
                CardView cardMatpelBg = (CardView) holder.itemView.findViewById(R.id.cardview_matpel);
                TextView namaMatpelText = (TextView) holder.itemView.findViewById(R.id.tv_nama_matpel);

                final Integer matpelId = item.getMatpelId();
                final String matpelNama = item.getNamaMatpel();

                namaMatpelText.setText(item.getNamaMatpel());
                cardMatpelBg.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Bundle bundle = new Bundle();
                        bundle.putInt(MainConst.KEY_INTENT_JENJANG_ID, getIntent().getExtras().getInt(MainConst.KEY_INTENT_JENJANG_ID));
                        bundle.putInt(MainConst.KEY_INTENT_KELAS_ID, getIntent().getExtras().getInt(MainConst.KEY_INTENT_KELAS_ID));
                        bundle.putInt(MainConst.KEY_INTENT_MATPEL_ID, matpelId);

                        bundle.putString(MainConst.KEY_INTENT_MATPEL_NAMA, matpelNama);
                        bundle.putString(MainConst.KEY_INTENT_JENJANG_NAMA,
                                getIntent().getExtras().getString(MainConst.KEY_INTENT_JENJANG_NAMA));
                        bundle.putString(MainConst.KEY_INTENT_KELAS_NAMA,
                                getIntent().getExtras().getString(MainConst.KEY_INTENT_KELAS_NAMA));

                        Intent levelIntent = new Intent(MatpelActivity.this, LevelActivity.class);
                        levelIntent.putExtras(bundle);
                        startActivity(levelIntent);
                        finish();
                    }
                });
            }
        });

        recyclerView.setLayoutManager(new GridLayoutManager(this, 2));
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(rvAdapter);
    }

    private void initRetrofit() {
        apiService = ApiAdapter.getInstance().getRetrofit(MainConst.BASE_URL).create(ApiService.class);
        callMatpelApi();
    }

    private void callMatpelApi() {
        stateView.setViewState(MultiStateView.VIEW_STATE_LOADING);
        Call<MatpelData> matpelCall = apiService.getMatpel();
        matpelCall.enqueue(new Callback<MatpelData>() {
            @Override
            public void onResponse(Call<MatpelData> call, Response<MatpelData> response) {
                List<Matpel> items = response.body().getListMatpel();

                if (response.isSuccessful()) {
                    stateView.setViewState(MultiStateView.VIEW_STATE_CONTENT);
                    rvAdapter.addAll(items);
                } else {
                    Timber.e("MESSAGE: " + response.message());
                    Timber.e("RESPONSE CODE " + response.code());
                    Toast.makeText(MatpelActivity.this, "Error response", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<MatpelData> call, Throwable t) {
                if (t instanceof SocketTimeoutException) {
                    Toast.makeText(MatpelActivity.this, "Request timeout. Please try again", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getApplicationContext(), "Network connecting error", Toast.LENGTH_SHORT).show();
                }

                stateView.setViewState(MultiStateView.VIEW_STATE_ERROR);

                Timber.e("ERROR: " + t.getMessage());
            }
        });
    }

    @Override
    public void onBackPressed() {
        backToKelasActivity();
    }

    private void backToKelasActivity() {
        Bundle bundle = new Bundle();
        bundle.putInt(MainConst.KEY_INTENT_JENJANG_ID,
                getIntent().getExtras().getInt(MainConst.KEY_INTENT_JENJANG_ID));
        bundle.putString(MainConst.KEY_INTENT_JENJANG_NAMA,
                getIntent().getExtras().getString(MainConst.KEY_INTENT_JENJANG_NAMA));

        Intent kelasIntent = new Intent(MatpelActivity.this, KelasActivity.class);
        kelasIntent.putExtras(bundle);
        startActivity(kelasIntent);
        finish();
    }
}
