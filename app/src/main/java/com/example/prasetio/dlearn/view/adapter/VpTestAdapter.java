package com.example.prasetio.dlearn.view.adapter;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.prasetio.dlearn.R;
import com.example.prasetio.dlearn.model.Soal;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Aldyaz on 7/25/2017.
 */

public class VpTestAdapter extends PagerAdapter {

    private Context context;
    private List<Soal> soalList;

    public VpTestAdapter(Context context) {
        this.context = context;
        soalList = new ArrayList<>();
    }

    @Override
    public int getCount() {
        return soalList.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View layoutView = inflater.inflate(R.layout.layout_soal, container, false);
        container.addView(layoutView);
        return layoutView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    public void addAllItem(List<Soal> item) {
        soalList.addAll(item);
        notifyDataSetChanged();
    }
}
