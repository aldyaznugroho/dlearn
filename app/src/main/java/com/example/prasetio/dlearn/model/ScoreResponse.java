package com.example.prasetio.dlearn.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Aldyaz on 8/1/2017.
 */

public class ScoreResponse implements Parcelable {
    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("score")
    @Expose
    private Integer score;
    @SerializedName("soal")
    @Expose
    private List<SoalResult> soalResultList;

    public ScoreResponse() {
    }

    protected ScoreResponse(Parcel in) {
    }

    public static final Creator<ScoreResponse> CREATOR = new Creator<ScoreResponse>() {
        @Override
        public ScoreResponse createFromParcel(Parcel in) {
            return new ScoreResponse(in);
        }

        @Override
        public ScoreResponse[] newArray(int size) {
            return new ScoreResponse[size];
        }
    };

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public Integer getScore() {
        return score;
    }

    public void setScore(Integer score) {
        this.score = score;
    }

    public List<SoalResult> getSoalResultList() {
        return soalResultList;
    }

    public void setSoalResultList(List<SoalResult> soalResultList) {
        this.soalResultList = soalResultList;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
    }
}
