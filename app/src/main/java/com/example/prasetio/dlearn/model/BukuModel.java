package com.example.prasetio.dlearn.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Aldyaz on 9/10/2017.
 */

public class BukuModel implements Parcelable {

    private String namaFile;
    private String jenjang;
    private String kelas;
    private String matpel;

    public BukuModel() {
    }

    protected BukuModel(Parcel in) {
        namaFile = in.readString();
        jenjang = in.readString();
        kelas = in.readString();
        matpel = in.readString();
    }

    public static final Creator<BukuModel> CREATOR = new Creator<BukuModel>() {
        @Override
        public BukuModel createFromParcel(Parcel in) {
            return new BukuModel(in);
        }

        @Override
        public BukuModel[] newArray(int size) {
            return new BukuModel[size];
        }
    };

    public String getNamaFile() {
        return namaFile;
    }

    public void setNamaFile(String namaFile) {
        this.namaFile = namaFile;
    }

    public String getJenjang() {
        return jenjang;
    }

    public void setJenjang(String jenjang) {
        this.jenjang = jenjang;
    }

    public String getKelas() {
        return kelas;
    }

    public void setKelas(String kelas) {
        this.kelas = kelas;
    }

    public String getMatpel() {
        return matpel;
    }

    public void setMatpel(String matpel) {
        this.matpel = matpel;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(namaFile);
        parcel.writeString(jenjang);
        parcel.writeString(kelas);
        parcel.writeString(matpel);
    }
}
