package com.example.prasetio.dlearn.activity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.prasetio.dlearn.R;
import com.example.prasetio.dlearn.model.JawabanSoalLog;
import com.example.prasetio.dlearn.model.ScoreResponse;
import com.example.prasetio.dlearn.utils.MainConst;
import com.example.prasetio.dlearn.utils.SharedPrefCustom;

import butterknife.BindView;
import butterknife.ButterKnife;
import github.nisrulz.screenshott.ScreenShott;
import timber.log.Timber;

public class SoalResultActivity extends AppCompatActivity {

    @BindView(R.id.soal_result_toolbar) Toolbar toolbar;
    @BindView(R.id.hasil_view) View hasilView;
    @BindView(R.id.share_fab) FloatingActionButton shareFAB;
    @BindView(R.id.result_total_nilai_tv) TextView totalNilaiTv;
    @BindView(R.id.result_jenjang_tv) TextView jenjangTv;
    @BindView(R.id.result_kelas_tv) TextView kelasTv;
    @BindView(R.id.result_matpel_tv) TextView matpelTv;
    @BindView(R.id.result_level_tv) TextView levelTv;
    @BindView(R.id.review_soal_button) Button reviewSoalButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_soal_result);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        init();
        setTitle(null);
    }

    private void init() {
        initToolbar();
        initResultCard();
        initShareFAB();
        initReviewButton();
    }

    private void initToolbar() {
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        Drawable navigationIcon = toolbar.getNavigationIcon();
        Drawable overflowIcon = toolbar.getOverflowIcon();
        if (overflowIcon != null && navigationIcon != null) {
            navigationIcon.mutate();
            navigationIcon.setColorFilter(
                    ContextCompat.getColor(this, R.color.color_white),
                    PorterDuff.Mode.SRC_ATOP);

            overflowIcon.mutate();
            overflowIcon.setColorFilter(
                    ContextCompat.getColor(this, R.color.color_white),
                    PorterDuff.Mode.SRC_ATOP);
        }
    }

    private void initShareFAB() {
        shareFAB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (checkPermissions()) {
                    showShareIntent();
                } else {
                    requestPermission();
                }
            }
        });
    }

    private void initResultCard() {
        String totalNilai = "" + getIntent().getExtras().getInt(MainConst.KEY_REVIEW_SOAL_SCORE) + "/100";

        jenjangTv.setText(getIntent().getExtras().getString(MainConst.KEY_REVIEW_SOAL_JENJANG));
        kelasTv.setText(getIntent().getExtras().getString(MainConst.KEY_REVIEW_SOAL_KELAS));
        matpelTv.setText(getIntent().getExtras().getString(MainConst.KEY_REVIEW_SOAL_MATPEL));
        levelTv.setText(getIntent().getExtras().getString(MainConst.KEY_REVIEW_SOAL_LEVEL));
        totalNilaiTv.setText(totalNilai);
    }

    private void initReviewButton() {
        reviewSoalButton.setPaintFlags(reviewSoalButton.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        reviewSoalButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle bundle = new Bundle();
                bundle.putString(MainConst.KEY_REVIEW_FIREBASE_RANDOM_KEY, getIntent().getExtras().getString(MainConst.KEY_REVIEW_FIREBASE_RANDOM_KEY));

                Intent reviewSoalIntent = new Intent(SoalResultActivity.this, ReviewSoalActivity.class);
                reviewSoalIntent.putExtras(bundle);
                startActivity(reviewSoalIntent);
            }
        });
    }

    private void showShareIntent() {
        Bitmap resultBitmap = ScreenShott.getInstance().takeScreenShotOfView(hasilView);
        String path = MediaStore.Images.Media.insertImage(
                SoalResultActivity.this.getContentResolver(),
                resultBitmap, null, null);

        Intent shareIntent = new Intent(Intent.ACTION_SEND);
        shareIntent.setType("image/jpg");
        shareIntent.putExtra(Intent.EXTRA_STREAM, Uri.parse(path));
        startActivity(Intent.createChooser(shareIntent, "Share using"));
    }

    private boolean checkPermissions() {
        return (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) |
                ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) ==
                PackageManager.PERMISSION_GRANTED;
    }

    private void requestPermission() {
        ActivityCompat.requestPermissions(this, new String[] {
                Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE},
                1000);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case 1000:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                    showShareIntent();
                else
                    Toast.makeText(this, "Cannot share the result", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}
