package com.example.prasetio.dlearn.utils;

/**
 * Created by Prasetio on 7/19/2017.
 */

public class MainConst {

    public static final String BASE_URL = "http://skripsi-pras.hasanbasri.net/api/";

    public static final String SOAL_ADAPTER_TYPE = "SOAL_ADAPTER_TYPE";
    public static final String REVIEW_ADAPTER_TYPE = "REVIEW_ADAPTER_TYPE";

    public static final String KEY_USER_ID = "user_id";
    public static final String KEY_USER_NAME = "user_name";
    public static final String KEY_USER_EMAIL = "user_email";
    public static final String KEY_USER_PROFILE_IMAGE = "user_profile_image";

    public static final String KEY_INTENT_JENJANG_ID = "key_intent_jenjang_id";
    public static final String KEY_INTENT_JENJANG_NAMA = "key_intent_jenjang_nama";
    public static final String KEY_INTENT_KELAS_ID = "key_intent_kelas_id";
    public static final String KEY_INTENT_KELAS_NAMA = "key_intent_kelas_nama";
    public static final String KEY_INTENT_KELAS_JENJANG = "key_intent_kelas_jenjang";
    public static final String KEY_INTENT_MATPEL_ID = "key_intent_matpel_id";
    public static final String KEY_INTENT_MATPEL_NAMA = "key_intent_matpel_nama";
    public static final String KEY_INTENT_LEVEL_ID = "key_intent_level_id";
    public static final String KEY_INTENT_LEVEL_NAMA = "key_intent_level_nama";

    public static final String KEY_INTENT_SOAL_VIEW_PHOTO = "key_intent_soal_view_photo";

    public static final String KEY_JAWABAN_LOG_LIST = "key_jawaban_log_list";
    public static final String KEY_JAWABAN_LOG_SOAL_ID = "key_jawaban_log_soal_id";
    public static final String KEY_JAWABAN_LOG_DIPILIH = "key_jawaban_log_dipilih";
    public static final String KEY_JAWABAN_LOG_BENAR = "key_jawaban_log_benar";
    public static final String KEY_JAWABAN_LOG_NILAI = "key_jawaban_log_nilai";

    public static final String KEY_TOTAL_NILAI = "key_total_nilai";
    public static final String KEY_REVIEW_FIREBASE_RANDOM_KEY = "key_review_firebase_random_key";
    public static final String KEY_REVIEW_SOAL_SCORE = "key_review_soal_score";
    public static final String KEY_REVIEW_SOAL_MATPEL = "key_review_soal_matpel";
    public static final String KEY_REVIEW_SOAL_JENJANG = "key_review_soal_jenjang";
    public static final String KEY_REVIEW_SOAL_LEVEL = "key_review_soal_level";
    public static final String KEY_REVIEW_SOAL_KELAS = "key_review_soal_kelas";

    public static final String KEY_LIST_JAWABAN = "key_list_jawaban";
    public static final String KEY_SOAL_ADAPTER_SIZE = "key_soal_adapter_size";

    public static final String KEY_MAP_MEMBER_ID = "member_id";
    public static final String KEY_MAP_MATPEL_ID = "matpel_id";
    public static final String KEY_MAP_LEVEL_ID = "level_id";
    public static final String KEY_MAP_JENJANG_ID = "jenjang_id";
    public static final String KEY_MAP_KELAS_ID = "kelas_id";
    public static final String KEY_MAP_LOG_JAWABAN = "log_jawaban";

    public static final String KEY_DB_SOAL_HISTORY = "dlearn_soal_history";

}
