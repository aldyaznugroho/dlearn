package com.example.prasetio.dlearn.activity;

import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.prasetio.dlearn.R;
import com.example.prasetio.dlearn.api.ApiAdapter;
import com.example.prasetio.dlearn.api.ApiService;
import com.example.prasetio.dlearn.model.Kelas;
import com.example.prasetio.dlearn.model.KelasData;
import com.example.prasetio.dlearn.utils.MainConst;
import com.example.prasetio.dlearn.view.adapter.CustomRecyclerAdapter;
import com.kennyc.view.MultiStateView;

import java.net.SocketTimeoutException;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

public class KelasActivity extends AppCompatActivity {

    @BindView(R.id.kelas_toolbar) Toolbar toolbar;
    @BindView(R.id.kelas_state_layout) MultiStateView stateView;
    @BindView(R.id.rv_kelas) RecyclerView recyclerView;

    private ApiService apiService;
    private CustomRecyclerAdapter<Kelas> rvAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kelas);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        init();
        setTitle(String.valueOf(getIntent().getExtras().getString(MainConst.KEY_INTENT_JENJANG_NAMA)).toUpperCase());
    }

    private void init() {
        initToolbar();
        initStateView();
        initRecyclerView();
        initRetrofit();
    }

    private void initToolbar() {
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        Drawable navigationIcon = toolbar.getNavigationIcon();
        Drawable overflowIcon = toolbar.getOverflowIcon();
        if (overflowIcon != null && navigationIcon != null) {
            navigationIcon.mutate();
            navigationIcon.setColorFilter(
                    ContextCompat.getColor(this, R.color.color_white),
                    PorterDuff.Mode.SRC_ATOP);

            overflowIcon.mutate();
            overflowIcon.setColorFilter(
                    ContextCompat.getColor(this, R.color.color_white),
                    PorterDuff.Mode.SRC_ATOP);
        }
    }

    private void initStateView() {
        stateView.getView(MultiStateView.VIEW_STATE_ERROR).findViewById(R.id.state_retry_button)
                .setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        callKelasApi();
                    }
                });
    }

    private void initRecyclerView() {
        rvAdapter = new CustomRecyclerAdapter<>(this, R.layout.item_class_holder, new CustomRecyclerAdapter.ViewHolderBinder<Kelas>() {
            @Override
            public void bind(CustomRecyclerAdapter.CustomViewHolder<Kelas> holder, Kelas item) {
                CardView kelasCardBg = (CardView) holder.itemView.findViewById(R.id.cardview_kelas);
                TextView kelasTextView = (TextView) holder.itemView.findViewById(R.id.tv_kelas);

                final Integer kelasId = item.getKelasId();
                final String kelasNama = item.getNamaKelas();
                final String kelasJenjang = item.getNamaJenjang();

                kelasTextView.setText(item.getNamaKelas());
                kelasCardBg.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Bundle bundle = new Bundle();
                        bundle.putInt(MainConst.KEY_INTENT_JENJANG_ID, getIntent().getExtras().getInt(MainConst.KEY_INTENT_JENJANG_ID));
                        bundle.putInt(MainConst.KEY_INTENT_KELAS_ID, kelasId);
                        bundle.putString(MainConst.KEY_INTENT_KELAS_NAMA, kelasNama);
                        bundle.putString(MainConst.KEY_INTENT_JENJANG_NAMA, kelasJenjang);

                        Intent kelasIntent = new Intent(KelasActivity.this, MatpelActivity.class);
                        kelasIntent.putExtras(bundle);
                        startActivity(kelasIntent);
                        finish();
                    }
                });
            }
        });

        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(rvAdapter);
    }

    private void initRetrofit() {
        apiService = ApiAdapter.getInstance().getRetrofit(MainConst.BASE_URL).create(ApiService.class);
        callKelasApi();
    }

    private void callKelasApi() {
        stateView.setViewState(MultiStateView.VIEW_STATE_LOADING);
        Call<KelasData> kelasDataCall = apiService.getKelas(getIntent().getExtras().getInt(MainConst.KEY_INTENT_JENJANG_ID));
        kelasDataCall.enqueue(new Callback<KelasData>() {
            @Override
            public void onResponse(Call<KelasData> call, Response<KelasData> response) {
                List<Kelas> items = response.body().getListKelas();

                if (response.isSuccessful()) {
                    stateView.setViewState(MultiStateView.VIEW_STATE_CONTENT);
                    rvAdapter.addAll(items);
                    Timber.d("SUKSES...");
                } else {
                    Timber.e("MESSAGE: " + response.message());
                    Timber.e("RESPONSE CODE " + response.code());
                    Toast.makeText(KelasActivity.this, "Error response", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<KelasData> call, Throwable t) {
                if (t instanceof SocketTimeoutException) {
                    Toast.makeText(KelasActivity.this, "Request timeout. Please try again", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getApplicationContext(), "Network connecting error", Toast.LENGTH_SHORT).show();
                }

                stateView.setViewState(MultiStateView.VIEW_STATE_ERROR);

                Timber.e("ERROR: " + t.getMessage());
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
