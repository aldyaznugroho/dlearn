package com.example.prasetio.dlearn.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Button;
import android.widget.Toast;

import com.example.prasetio.dlearn.R;
import com.example.prasetio.dlearn.api.ApiAdapter;
import com.example.prasetio.dlearn.api.ApiService;
import com.example.prasetio.dlearn.model.User;
import com.example.prasetio.dlearn.model.UserData;
import com.example.prasetio.dlearn.utils.MainConst;
import com.example.prasetio.dlearn.utils.SharedPrefCustom;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.GoogleAuthProvider;

import java.net.SocketTimeoutException;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

public class LoginActivity extends AppCompatActivity {

    public static final int RC_SIGN_IN = 1;

    @BindView(R.id.google_login_button) Button googleLoginButton;

    private ProgressDialog progressDialog;

    private FirebaseAuth firebaseAuth;
    protected GoogleSignInOptions googleSignInOptions;
    protected GoogleApiClient googleApiClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        init();
    }

    private void init() {
        initProgressDialog();
        initFirebase();
        initGoogleLogin();
    }

    private void initFirebase() {
        firebaseAuth = FirebaseAuth.getInstance();
    }

    private void initGoogleLogin() {
        googleSignInOptions = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();
        googleApiClient = new GoogleApiClient.Builder(getBaseContext())
                .enableAutoManage(this, new GoogleApiClient.OnConnectionFailedListener() {
                    @Override
                    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
                    }
                })
                .addApi(Auth.GOOGLE_SIGN_IN_API, googleSignInOptions)
                .build();
    }

    private void initProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Mohon tunggu...");
        progressDialog.setCancelable(false);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            if (result.isSuccess()) {
                showProgress();
                GoogleSignInAccount account = result.getSignInAccount();
                handleGoogleAccessToken(account);
            } else {
                Log.e("SIGNINRESULT", "" + result.getStatus());
                Toast.makeText(getApplicationContext(), "Google login gagal", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void handleGoogleAccessToken(GoogleSignInAccount account) {
        final AuthCredential credential = GoogleAuthProvider.getCredential(account.getIdToken(), null);

        final String userName = account.getDisplayName();
        final String userEmail = account.getEmail();
        final String userProfilePict = String.valueOf(account.getPhotoUrl());

        firebaseAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (!task.isSuccessful()) {
                            dismissProgress();
                            Log.e("AuthActivity", String.valueOf(task.getException()));
                        } else {
                            Map<String, String> formFields = new HashMap<>();
                            formFields.put("nama", userName);
                            formFields.put("email", userEmail);
                            formFields.put("profile_picture", userProfilePict);

                            ApiService apiService = ApiAdapter.getInstance().getRetrofit(MainConst.BASE_URL).create(ApiService.class);
                            Call<UserData> userDataCall = apiService.postLogin(formFields);
                            userDataCall.enqueue(new Callback<UserData>() {
                                @Override
                                public void onResponse(Call<UserData> call, Response<UserData> response) {
                                    User user = response.body().getUser();

                                    if (response.isSuccessful()) {
                                        dismissProgress();
                                        SharedPrefCustom.getInstance(LoginActivity.this).putSharedPrefUser(user);
                                        goToHomeActivity();
                                    } else {
                                        dismissProgress();
                                        Timber.e("MESSAGE: " + response.message());
                                        Timber.e("RESPONSE CODE " + response.code());
                                        Toast.makeText(LoginActivity.this, "Error response", Toast.LENGTH_SHORT).show();
                                    }
                                }

                                @Override
                                public void onFailure(Call<UserData> call, Throwable t) {
                                    if (t instanceof SocketTimeoutException) {
                                        Toast.makeText(LoginActivity.this, "Request timeout. Please try again", Toast.LENGTH_SHORT).show();
                                    } else {
                                        Toast.makeText(getApplicationContext(), "Network connecting error", Toast.LENGTH_SHORT).show();
                                    }

                                    dismissProgress();

                                    Timber.e("ERROR: " + t.getMessage());
                                }
                            });
                        }
                    }
                });
    }

    private void goToHomeActivity() {
        Intent homeIntent = new Intent(getApplicationContext(), HomeActivity.class);
        homeIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(homeIntent);
        finish();
    }

    @OnClick(R.id.google_login_button)
    public void clickGoogleLoginButton() {
        startGoogleSignInIntent();
    }

    public void startGoogleSignInIntent() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(googleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    /**
     * Showing progress dialog
     */
    public void showProgress() {
        progressDialog.show();
    }

    /**
     * Dismiss progress dialog
     */
    public void dismissProgress() {
        progressDialog.dismiss();
    }
}
