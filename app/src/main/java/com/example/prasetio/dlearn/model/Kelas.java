package com.example.prasetio.dlearn.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Prasetio on 7/19/2017.
 */

public class Kelas {
    @SerializedName("id")
    @Expose
    private Integer kelasId;
    @SerializedName("nama_kelas")
    @Expose
    private String namaKelas;

    @SerializedName("nama_jenjang")
    @Expose
    private String namaJenjang;

    public Integer getKelasId() {
        return kelasId;
    }

    public void setKelasId(Integer kelasId) {
        this.kelasId = kelasId;
    }

    public String getNamaKelas() {
        return namaKelas;
    }

    public void setNamaKelas(String namaKelas) {
        this.namaKelas = namaKelas;
    }

    public String getNamaJenjang() {
        return namaJenjang;
    }

    public void setNamaJenjang(String namaJenjang) {
        this.namaJenjang = namaJenjang;
    }
}
