package com.example.prasetio.dlearn.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Aldyaz on 7/24/2017.
 */

public class OpsiJawaban {

    private Integer opsiId;

    private Integer soalId;

    @SerializedName("text")
    @Expose
    private String text;

    @SerializedName("picture")
    @Expose
    private String picture;

    public OpsiJawaban(Integer soalId, String text,
            String picture) {
        this.soalId = soalId;
        this.text = text;
        this.picture = picture;
    }

    public OpsiJawaban() {
    }

    public Integer getOpsiId() {
        return opsiId;
    }

    public void setOpsiId(Integer opsiId) {
        this.opsiId = opsiId;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public Integer getSoalId() {
        return this.soalId;
    }

    public void setSoalId(Integer soalId) {
        this.soalId = soalId;
    }
}
