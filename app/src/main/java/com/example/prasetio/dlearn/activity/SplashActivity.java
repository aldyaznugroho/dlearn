package com.example.prasetio.dlearn.activity;

import android.app.Activity;
import android.content.Intent;

import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.Handler;

import com.example.prasetio.dlearn.R;
import com.google.firebase.auth.FirebaseAuth;

public class SplashActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (FirebaseAuth.getInstance().getCurrentUser() != null) {
                    Intent homeIntent = new Intent(SplashActivity.this, HomeActivity.class);
                    startActivity(homeIntent);
                    finish();
                } else {
                    Intent authIntent = new Intent(SplashActivity.this, LoginActivity.class);
                    startActivity(authIntent);
                    finish();
                }
            }
        }, 2000);
    }
}
