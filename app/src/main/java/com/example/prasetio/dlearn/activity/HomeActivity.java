package com.example.prasetio.dlearn.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.example.prasetio.dlearn.fragment.BukuFragment;
import com.example.prasetio.dlearn.fragment.JenjangFragment;
import com.example.prasetio.dlearn.R;
import com.example.prasetio.dlearn.model.User;
import com.example.prasetio.dlearn.utils.SharedPrefCustom;
import com.example.prasetio.dlearn.view.adapter.CustomTabPagerAdapter;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.firebase.auth.UserInfo;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import timber.log.Timber;

public class HomeActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    @BindView(R.id.main_toolbar) Toolbar toolbar;
    @BindView(R.id.main_drawer) DrawerLayout drawerLayout;
    @BindView(R.id.home_tab_layout) TabLayout tabLayout;
    @BindView(R.id.home_tab_view_pager) ViewPager viewPager;
    @BindView(R.id.main_navigation_view) NavigationView navigationView;

    private FirebaseAuth firebaseAuth;
    private FirebaseUser firebaseUser;
    protected FirebaseAuth.AuthStateListener authStateListener;
    protected GoogleSignInOptions googleSignInOptions;
    protected GoogleApiClient googleApiClient;

    private ActionBarDrawerToggle drawerToggle;
    private CustomTabPagerAdapter tabPagerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        init();
        setTitle("DLearn");
    }

    private void init() {
        initToolbar();
        initTabLayout();
        initNavigationDrawer();
        initFirebaseAuth();
        initGoogleLogin();
    }

    private void initToolbar() {
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawerLayout.openDrawer(GravityCompat.START);
            }
        });

        Drawable navigationIcon = toolbar.getNavigationIcon();
        Drawable overflowIcon = toolbar.getOverflowIcon();
        if (overflowIcon != null && navigationIcon != null) {
            navigationIcon.mutate();
            navigationIcon.setColorFilter(
                    ContextCompat.getColor(this, R.color.color_white),
                    PorterDuff.Mode.SRC_ATOP);

            overflowIcon.mutate();
            overflowIcon.setColorFilter(
                    ContextCompat.getColor(this, R.color.color_white),
                    PorterDuff.Mode.SRC_ATOP);
        }
    }

    private void initTabLayout() {
        tabPagerAdapter = new CustomTabPagerAdapter(getSupportFragmentManager());
        tabPagerAdapter.addTab(JenjangFragment.getInstance(), "Exercise");
        tabPagerAdapter.addTab(BukuFragment.getInstance(), "Learn");
        viewPager.setOffscreenPageLimit(2);
        viewPager.setAdapter(tabPagerAdapter);
        tabLayout.setupWithViewPager(viewPager);
    }

    private void initNavigationDrawer() {
        drawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar,
                R.string.navigation_drawer_open,
                R.string.navigation_drawer_close) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
            }
        };

        drawerLayout.addDrawerListener(drawerToggle);
        navigationView.setNavigationItemSelectedListener(this);

        View navHeaderView = navigationView.getHeaderView(0);
        ImageView drawerProfileImg = (ImageView) navHeaderView.findViewById(R.id.drawer_profile_image);
        TextView drawerName = (TextView) navHeaderView.findViewById(R.id.drawer_user_name);
        TextView drawerEmail = (TextView) navHeaderView.findViewById(R.id.drawer_user_email);

        User userPref = SharedPrefCustom.getInstance(this).getSharedPrefUser();

        drawerName.setText(userPref.getNama());
        drawerEmail.setText(userPref.getEmail());

        Uri profileUri = Uri.parse(userPref.getProfilePicture());
        Glide.with(this)
                .fromUri()
                .load(profileUri)
                .diskCacheStrategy(DiskCacheStrategy.RESULT)
                .skipMemoryCache(true)
                .dontAnimate()
                .placeholder(R.drawable.dummy_profile_1)
                .error(R.drawable.dummy_profile_1)
                .into(drawerProfileImg);

    }

    private void initFirebaseAuth() {
        firebaseAuth = FirebaseAuth.getInstance();
        firebaseUser = firebaseAuth.getCurrentUser();
        authStateListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                if (firebaseAuth.getCurrentUser() == null) {
                    Intent loginIntent = new Intent(HomeActivity.this, LoginActivity.class);
                    loginIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(loginIntent);
                    finish();
                }
            }
        };
    }

    private void initGoogleLogin() {
        googleSignInOptions = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();
        googleApiClient = new GoogleApiClient.Builder(getBaseContext())
                .enableAutoManage(this, new GoogleApiClient.OnConnectionFailedListener() {
                    @Override
                    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
                    }
                })
                .addApi(Auth.GOOGLE_SIGN_IN_API, googleSignInOptions)
                .build();
    }

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
        } else {
            finishAffinity();
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.nav_history:
                drawerLayout.closeDrawer(GravityCompat.START);
                Intent historyIntent = new Intent(HomeActivity.this, HistorySoalActivity.class);
                startActivity(historyIntent);
                break;

            case R.id.nav_help:
                drawerLayout.closeDrawer(GravityCompat.START);
                Intent helpIntent = new Intent(HomeActivity.this, HelpActivity.class);
                startActivity(helpIntent);
                break;

            case R.id.nav_about:
                drawerLayout.closeDrawer(GravityCompat.START);
                Intent aboutIntent = new Intent(HomeActivity.this, AboutActivity.class);
                startActivity(aboutIntent);
                break;

            case R.id.nav_logout:
                drawerLayout.closeDrawer(GravityCompat.START);
                AlertDialog.Builder logoutBuilder = new AlertDialog.Builder(this);
                AlertDialog logoutDialog;

                logoutBuilder.setMessage("Anda yakin ingin logout?");
                logoutBuilder.setPositiveButton("Logout", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        logout();
                    }
                });
                logoutBuilder.setNegativeButton("Batal", null);
                logoutDialog = logoutBuilder.create();
                logoutDialog.show();
                break;
        }

        return false;
    }

    private void logout() {
        for (UserInfo userInfo : firebaseUser.getProviderData()) {
            if (userInfo.getProviderId().equals(GoogleAuthProvider.PROVIDER_ID)) {
                firebaseAuth.signOut();
                Auth.GoogleSignInApi.signOut(googleApiClient).setResultCallback(
                        new ResultCallback<Status>() {
                            @Override
                            public void onResult(@NonNull Status status) {
                                SharedPrefCustom.getInstance(HomeActivity.this).removeSharedPrefUser();

                                Intent loginIntent = new Intent(HomeActivity.this, LoginActivity.class);
                                loginIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(loginIntent);
                                finish();
                            }
                        }
                );
            }
        }
    }
}
