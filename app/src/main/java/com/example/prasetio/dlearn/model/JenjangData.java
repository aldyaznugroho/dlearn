package com.example.prasetio.dlearn.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Prasetio on 7/19/2017.
 */

public class JenjangData {
    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("total")
    @Expose
    private Integer total;
    @SerializedName("data")
    @Expose
    private List<Jenjang> listJenjang = new ArrayList<Jenjang>();

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public List<Jenjang> getListJenjang() {
        return listJenjang;
    }

    public void setListJenjang(List<Jenjang> listJenjang) {
        this.listJenjang = listJenjang;
    }
}
