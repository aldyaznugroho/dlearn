package com.example.prasetio.dlearn.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Prasetio on 7/19/2017.
 */

public class Jenjang {

    @SerializedName("id")
    @Expose
    private Integer jenjangId;

    @SerializedName("nama_jenjang")
    @Expose
    private String namaJenjang;

    public Integer getJenjangId() {
        return jenjangId;
    }

    public void setJenjangId(Integer jenjangId) {
        this.jenjangId = jenjangId;
    }

    public String getNamaJenjang() {
        return namaJenjang;
    }

    public void setNamaJenjang(String namaJenjang) {
        this.namaJenjang = namaJenjang;
    }
}
