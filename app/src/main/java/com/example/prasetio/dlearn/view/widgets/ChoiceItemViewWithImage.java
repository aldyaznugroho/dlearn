package com.example.prasetio.dlearn.view.widgets;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.os.Parcelable;
import android.support.annotation.AttrRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StyleRes;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.example.prasetio.dlearn.R;
import com.example.prasetio.dlearn.model.OpsiJawaban;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Aldyaz on 7/23/2017.
 */

public class ChoiceItemViewWithImage extends FrameLayout {

    @BindView(R.id.opsi_bg) CardView opsiCardView;
    @BindView(R.id.opsi_answer_iv) ImageView opsiAnswerImage;
    @BindView(R.id.opsi_index_tv) TextView opsiIndexText;
    @BindView(R.id.opsi_answer_tv) TextView opsiAnswerText;

    private String[] choiceIndex = {"A", "B", "C", "D"};

    public ChoiceItemViewWithImage(@NonNull Context context) {
        super(context);
        initViews();
    }

    public ChoiceItemViewWithImage(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public ChoiceItemViewWithImage(@NonNull Context context, @Nullable AttributeSet attrs, @AttrRes int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public ChoiceItemViewWithImage(@NonNull Context context, @Nullable AttributeSet attrs, @AttrRes int defStyleAttr, @StyleRes int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    private void initViews() {
        View view = inflate(getContext(), R.layout.choice_item_view_with_image, this);
        RecyclerView.LayoutParams layoutParams = new RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        view.setLayoutParams(layoutParams);
        ButterKnife.bind(this, view);
    }

    public void bind(OpsiJawaban opsi, int position, boolean isToggled) {
        if (opsi != null) {
            bindSoalLayout(opsi, position, isToggled);
        }
    }

    private void bindSoalLayout(OpsiJawaban opsi, int position, boolean isToggled) {
        if (isToggled) {
            cardBackgroundBlue();
        } else {
            cardBackgroundNormal();
        }

        Glide.with(getContext())
                .load(opsi.getPicture())
                .dontAnimate()
                .into(opsiAnswerImage);

        opsiIndexText.setText(choiceIndex[position]);

        opsiAnswerText.setText(opsi.getText());
        opsiAnswerText.setVisibility(GONE);
    }

    private void cardBackgroundNormal() {
        opsiAnswerText.setTextColor(ContextCompat.getColor(getContext(), R.color.color_black));
        opsiCardView.setCardBackgroundColor(ContextCompat.getColor(getContext(), R.color.color_white));
    }

    private void cardBackgroundBlue() {
        opsiAnswerText.setTextColor(ContextCompat.getColor(getContext(), R.color.color_white));
        opsiCardView.setCardBackgroundColor(ContextCompat.getColor(getContext(), R.color.color_blue_800));
    }
}
