package com.example.prasetio.dlearn.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Aldyaz on 8/1/2017.
 */

public class ScoreBody {

    private Integer member_id;
    private Integer matpel_id;
    private Integer level_id;
    private Integer jenjang_id;
    private Integer kelas_id;
    private List<JawabanSoalLog> log_jawaban;

    public ScoreBody() {
    }

    public Integer getMember_id() {
        return member_id;
    }

    public void setMember_id(Integer member_id) {
        this.member_id = member_id;
    }

    public Integer getMatpel_id() {
        return matpel_id;
    }

    public void setMatpel_id(Integer matpel_id) {
        this.matpel_id = matpel_id;
    }

    public Integer getLevel_id() {
        return level_id;
    }

    public void setLevel_id(Integer level_id) {
        this.level_id = level_id;
    }

    public Integer getJenjang_id() {
        return jenjang_id;
    }

    public void setJenjang_id(Integer jenjang_id) {
        this.jenjang_id = jenjang_id;
    }

    public Integer getKelas_id() {
        return kelas_id;
    }

    public void setKelas_id(Integer kelas_id) {
        this.kelas_id = kelas_id;
    }

    public List<JawabanSoalLog> getLog_jawaban() {
        return log_jawaban;
    }

    public void setLog_jawaban(List<JawabanSoalLog> log_jawaban) {
        this.log_jawaban = log_jawaban;
    }
}
