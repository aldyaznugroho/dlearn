package com.example.prasetio.dlearn.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Aldyaz on 7/20/2017.
 */

public class Tingkatan {

    @SerializedName("id")
    @Expose
    private Integer levelId;
    @SerializedName("nama_level")
    @Expose
    private String namaLevel;

    public Integer getLevelId() {
        return levelId;
    }

    public void setLevelId(Integer levelId) {
        this.levelId = levelId;
    }

    public String getNamaLevel() {
        return namaLevel;
    }

    public void setNamaLevel(String namaLevel) {
        this.namaLevel = namaLevel;
    }
}
