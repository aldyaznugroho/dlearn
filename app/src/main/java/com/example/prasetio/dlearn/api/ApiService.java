package com.example.prasetio.dlearn.api;

import com.example.prasetio.dlearn.model.JenjangData;
import com.example.prasetio.dlearn.model.KelasData;
import com.example.prasetio.dlearn.model.MatpelData;
import com.example.prasetio.dlearn.model.ScoreResponse;
import com.example.prasetio.dlearn.model.ScoreBody;
import com.example.prasetio.dlearn.model.SoalData;
import com.example.prasetio.dlearn.model.TingkatanData;
import com.example.prasetio.dlearn.model.UserData;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * Created by Prasetio on 7/19/2017.
 */

public interface ApiService {

    @FormUrlEncoded
    @POST("login")
    Call<UserData> postLogin(@FieldMap Map<String, String> fields);

    @GET("jenjang")
    Call<JenjangData> getJenjang();

    @GET("kelas/{jenjang_id}")
    Call<KelasData> getKelas(@Path("jenjang_id") int jenjangId);

    @GET("level")
    Call<TingkatanData> getLevel();

    @GET("matapelajaran")
    Call<MatpelData> getMatpel();

    @GET("soal/{kelas_id}/{matpel_id}/{level_id}")
    Call<SoalData> getSoal(@Path("kelas_id") Integer kelasId,
                           @Path("matpel_id") Integer matpelId,
                           @Path("level_id") Integer levelId);

    @POST("hasil")
    Call<ScoreResponse> postScore(@Body ScoreBody scoreBody);

}
