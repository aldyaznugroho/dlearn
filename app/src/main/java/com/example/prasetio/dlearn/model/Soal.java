package com.example.prasetio.dlearn.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Aldyaz on 7/23/2017.
 */

public class Soal {

    @SerializedName("id")
    @Expose
    private Integer soalId;

    @SerializedName("mata_pelajaran")
    @Expose
    private String mataPelajaran;

    @SerializedName("kelas")
    @Expose
    private String kelas;

    @SerializedName("jenjang")
    @Expose
    private String jenjang;

    @SerializedName("soal")
    @Expose
    private String soal;

    @SerializedName("picture")
    @Expose
    private String picture;

    @SerializedName("opsi_jawaban")
    @Expose
    private List<OpsiJawaban> opsiJawaban;

    @SerializedName("jawaban_benar")
    @Expose
    private String jawabanBenar;

    @SerializedName("pembahasan_soal")
    @Expose
    private String pembahasanSoal;

    public Integer getSoalId() {
        return soalId;
    }

    public void setSoalId(Integer soalId) {
        this.soalId = soalId;
    }

    public String getMataPelajaran() {
        return mataPelajaran;
    }

    public void setMataPelajaran(String mataPelajaran) {
        this.mataPelajaran = mataPelajaran;
    }

    public String getKelas() {
        return kelas;
    }

    public void setKelas(String kelas) {
        this.kelas = kelas;
    }

    public String getJenjang() {
        return jenjang;
    }

    public void setJenjang(String jenjang) {
        this.jenjang = jenjang;
    }

    public String getSoal() {
        return soal;
    }

    public void setSoal(String soal) {
        this.soal = soal;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public List<OpsiJawaban> getOpsiJawaban() {
        return opsiJawaban;
    }

    public void setOpsiJawaban(List<OpsiJawaban> opsiJawaban) {
        this.opsiJawaban = opsiJawaban;
    }

    public String getJawabanBenar() {
        return jawabanBenar;
    }

    public void setJawabanBenar(String jawabanBenar) {
        this.jawabanBenar = jawabanBenar;
    }

    public String getPembahasanSoal() {
        return pembahasanSoal;
    }

    public void setPembahasanSoal(String pembahasanSoal) {
        this.pembahasanSoal = pembahasanSoal;
    }
}
